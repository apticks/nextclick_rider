<div class="row">
						<div class="col-xl-3  col-md-4 col-sm-4">
							<a href="<?php echo base_url('own_vehicle/r');?>">
    							<div class="card">
    								<div class="card-bg">
    									<div class="p-t-20 d-flex justify-content-between">
    										<div class="col">
    											<h6 class="mb-0">Own vehicle</h6>
    											<span class="font-weight-bold mb-0 font-20"></span>
    										</div>
    									</div>
    									<!-- <canvas id="cardChart4" height="80"></canvas> -->
    									<br/>
    									<div class="alert alert-sm alert-primary "><center><b><i class="fas fa-hand-holding-usd card-icon font-30 p-r-30"><?php echo $this->db->get_where('riders', ['own_vehicle' =>1, 'deleted_at' => NULL])->num_rows();?></i></b></center></div>
    								</div>
    							</div>
							</a>
						</div>
						<div class="col-xl-3  col-md-4 col-sm-4">
							<a href="<?php echo base_url('no_vehicle/r');?>">
    							<div class="card">
    								<div class="card-bg">
    									<div class="p-t-20 d-flex justify-content-between">
    										<div class="col">
    											<h6 class="mb-0">No vehicle</h6>
    											<span class="font-weight-bold mb-0 font-20"></span>
    										</div>
    									</div>
    									<!-- <canvas id="cardChart4" height="80"></canvas> -->
    									<br/>
    									<div class="alert alert-sm alert-primary "><center><b><i class="fas fa-hand-holding-usd card-icon font-30 p-r-30"></i><?php echo $this->db->get_where('riders', ['own_vehicle' =>0, 'deleted_at' => NULL])->num_rows();?></b></center></div>
    								</div>
    							</div>
							</a>
						</div>
							</div>
						</div>
						
					</div>
