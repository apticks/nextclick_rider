<?php
require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';

use Firebase\JWT\JWT;

class Master extends MY_REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('vendor_list_model');
        $this->load->model('news_model');
        $this->load->model('users_address_model');
        $this->load->model('location_model');
        $this->load->model('setting_model');
        $this->load->model('user_service_model');
    }

    /**
     * To get list of vendor depends upon category, search & near by location
     *
     * @author Mehar
     * @param integer $limit
     * @param integer $offset
     * @param integer $cat_id
     */
    public function vendor_list_get($cat_id = 1, $limit = 10, $offset = 0)
    {
        if (! empty($cat_id)) {
            $data = $this->vendor_list_model->all($limit, $offset, $cat_id, (isset($_GET['sub_cat_id'])) ? $this->input->get('sub_cat_id') : NUll);
            if (! empty($data['result'])) {
                foreach ($data['result'] as $d) {
                    $d->image = base_url() . 'uploads/list_cover_image/list_cover_' . $d->id . '.jpg';
                }
            }
            $this->set_response_simple((empty($data['result'])) ? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        } else {
            $this->set_response_simple(NULL, 'Please provide valid data..!', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
        }
    }

    /**
     * User address
     *
     * To Manage address
     *
     * @author Trupti
     * @param string $type
     */
    public function user_address_post($type = 'r')
    {
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        if ($type == 'c') {
            $this->form_validation->set_rules($this->users_address_model->rules);
            if ($this->form_validation->run() == false) {
                $this->set_response_simple(validation_errors(), 'Validation Error', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
                $v = $this->location_model->where('latitude', $this->input->post('latitude'))
                    ->where('longitude', $this->input->post('longitude'))
                    ->get();
                if ($v != '') {
                    $l_id = $v['id'];
                } else {
                    $l_id = $this->location_model->insert([
                        'latitude' => $this->input->post('latitude'),
                        'longitude' => $this->input->post('longitude'),
                        'address' => $this->input->post('address')
                    ]);
                }

                $id = $this->users_address_model->insert([
                    'user_id' => $token_data->id,
                    'name' => $this->input->post('name'),
                    'phone' => $this->input->post('phone'),
                    'email' => $this->input->post('email'),
                    'address' => $this->input->post('address'),
                    'location_id' => $l_id
                ]);
                $this->set_response_simple($id, 'Success..!', REST_Controller::HTTP_CREATED, TRUE);
            }
        } elseif ($type == 'r') {
            $data = $this->users_address_model->fields('id, user_id, name, phone, email, address, location_id')->get_all('user_id', $token_data->id);
            $this->set_response_simple(($data == FALSE) ? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        } elseif ($type == 's') {
            $data = $this->users_address_model->fields('id, user_id, name, phone, email, address, location_id')->get('id', $this->input->post('id'));
            $this->set_response_simple(($data == FALSE) ? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        } elseif ($type == 'u') {
            $this->form_validation->set_rules($this->users_address_model->rules);
            if ($this->form_validation->run() == FALSE) {
                $this->set_response_simple(validation_errors(), 'Validation Error', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
                $v = $this->location_model->where('latitude', $this->input->post('latitude'))
                    ->where('longitude', $this->input->post('longitude'))
                    ->get();
                if ($v != '') {
                    $l_id = $v['id'];
                } else {
                    $l_id = $this->location_model->insert([
                        'latitude' => $this->input->post('latitude'),
                        'longitude' => $this->input->post('longitude'),
                        'address' => $this->input->post('address')
                    ]);
                }
                $ll = $this->users_address_model->update([
                    'id' => $this->input->post('id'),
                    'user_id' => $token_data->id,
                    'name' => $this->input->post('name'),
                    'phone' => $this->input->post('phone'),
                    'email' => $this->input->post('email'),
                    'address' => $this->input->post('address'),
                    'location_id' => $l_id
                ], 'id');
                $this->set_response_simple($ll, 'Success..!', REST_Controller::HTTP_ACCEPTED, TRUE);
            }
        } elseif ($type == 'd') {
            $ll = $this->users_address_model->delete([
                'id' => $this->input->post('id')
            ]);
            $this->set_response_simple($ll, 'Deleted..!', REST_Controller::HTTP_OK, TRUE);
        }
    }

    /**
     * To get the individual vendor details
     *
     * @author Mehar
     * @param number $target
     */
    public function vendor_get($target = 1)
    {
        $data = $this->vendor_list_model->with_location('fields: id, address, latitude, longitude')
            ->with_category('fields: id, name')
            ->with_constituency('fields: id, name, state_id, district_id')
            ->with_contacts('fields: id, std_code, number, type')
            ->with_links('fields: id, 	url, type')
            ->with_amenities('fields: id, name')
            ->with_services('fields: id, name')
            ->with_holidays('fields: id')
            ->where('id', $target)
            ->get();
            $data['user_services'] = $this->user_service_model->order_by('id', 'DESC')->where('vendor_id', $data['vendor_user_id'])->get_all();
        if ($data != FALSE) {
            $data['banners'] = [];
            for ($i = 0; $i < $data['no_of_banners']; $i) {
                $data['banners'][$i] = base_url() . "uploads/list_banner_image/list_banner_" . $data['id'] . "_" . ++ $i . ".jpg";
            }
        }
        $this->set_response_simple($data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }

    /**
     * To get the news
     *
     * @author Mehar
     * @param number $limit,offset
     */
    public function news_get($limit = 10, $offset = 0)
    {
        $data = $this->news_model->all($limit, $offset);
        if (! empty($data['result'])) {
            foreach ($data['result'] as $d) {
                $d->image = base_url() . 'uploads/news_image/news_' . $d->id . '.jpg';
            }
        }
        $this->set_response_simple((empty($data['result'])) ? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }

    /**
     * To get the User Dettails
     *
     * @author Mahesh
     *        
     */
    public function user_details_get()
    {
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        $this->load->model('user_model');
        $result = $this->user_model->order_by('id', 'DESC')
            ->fields('id,unique_id,first_name,last_name,email,phone, wallet')
            ->with_groups('fields:name,id')
            ->where('id', $token_data->id)
            ->get();
        $this->set_response_simple(($result == FALSE) ? FALSE : $result, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }
    
    public function payment_settings_get(){
        $result['pay_per_vendor'] = $this->setting_model->where('key','pay_per_vendor')->get()['value'];
        $result['vendor_validation'] = $this->setting_model->where('key','vendor_validation')->get()['value'];
        $this->set_response_simple(($result == FALSE) ? FALSE : $result, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }
   
}

