<?php
require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';
use Firebase\JWT\JWT;
class Food extends MY_REST_Controller
{
   public function __construct()
   {
       parent::__construct();
       $this->load->model('food_menu_model');
       $this->load->model('food_item_model');
       $this->load->model('food_section_model');
       $this->load->model('food_sec_item_model');
       $this->load->model('food_orders_model');
       $this->load->model('food_order_items_model');
       $this->load->model('food_sub_order_items_model');
       $this->load->model('food_order_deal_model');
       $this->load->model('food_settings_model');
       $this->load->model('delivery_boy_status_model');
       $this->load->model('users_address_model');

       $this->load->model('vendor_list_model');
       /*$this->load->model('food_cart_model');*/
   }
   /**
    * @author Mahesh
    * @desc To get list of Menus and targeted Menu as well
    * @param string $target
    */
   public function food_menus_get($vendor_id) {
       if(! empty($vendor_id)){
           $data = $this->food_menu_model->fields('id,name,desc')->where('vendor_id', $vendor_id)->get_all();
           if(! empty($data)){
               for ($i = 0; $i < count($data) ; $i++){
                   $data[$i]['image'] = base_url().'uploads/food_menu_image/food_menu_'.$data[$i]['id'].'.jpg';
               }
           }
           $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
         }
   }
   /**
    * @author Mahesh
    * @desc To Check Vendor Avaliable status
    * @param string $vendor_id
    */
   public function FoodSettings_get($vendor_id) {
       if(! empty($vendor_id)){
           $data = $this->food_settings_model->fields('id,min_order_price,delivery_free_range,preparation_time,min_delivery_fee,ext_delivery_fee,restaurant_status')->where('vendor_id', $vendor_id)->get();
           if(! empty($data)){
            $data['image'] = base_url().'uploads/list_cover_image/list_cover_'.$data['id'].'.jpg';
           }
           $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
         }
   }
   /**
    * @author Mahesh
    * @desc To get list of Items
    * @param string $target
    */
   public function food_items_get($menu_id) {
       if(!empty($menu_id)){
        if(!empty($this->get('item_type'))){
          $this->db->where('item_type',$this->get('item_type'));
        }
           $data = $this->food_item_model->fields('id,name,desc,price,discount,quantity,item_type,status')->where('menu_id', $menu_id)->get_all();
           if(! empty($data)){
               for ($i = 0; $i < count($data) ; $i++){
                   $data[$i]['image'] = base_url().'uploads/food_item_image/food_item_'.$data[$i]['id'].'.jpg';
                   $data[$i]['item_status']= ($data[$i]['status']==1)? 'Available' : 'Not Available' ;
                   if($data[$i]['discount']>0){
                   $data[$i]['discount_price']=$data[$i]['price']-($data[$i]['price']*($data[$i]['discount']/100));
                    }
               }
               $res['result']=$data;
           }
           $res['item_type']=['1'=>'Veg','2'=>'Non-Veg'];
           $this->set_response_simple(($res == FALSE)? FALSE : $res, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
       
   }


   /**
    * @author Mahesh
    * @desc To get list of Items
    * @param string $target
    */
   public function FoodItems_get($vendor_id) {
    if(! empty($vendor_id)){
           $menus = $this->food_menu_model->fields('id,name,desc')->where('vendor_id', $vendor_id)->get_all();
           $list=array();
           $k=0;
           $res=array();
           if(!empty($menus)){
            $j=0;foreach ($menus as $menu) {
              $menu_id=$menu['id'];
                  if(!empty($menu_id)){
                  $data = $this->food_item_model->fields('id,name,desc,price,discount,quantity,item_type,status')->where('menu_id', $menu_id)->get_all();
                  if(! empty($data)){
                  for ($i = 0; $i < count($data) ; $i++){
                  $data[$i]['image'] = base_url().'uploads/food_item_image/food_item_'.$data[$i]['id'].'.jpg';
                  $data[$i]['item_status']= ($data[$i]['status']==1)? 'Available' : 'Not Available' ;
                  if($data[$i]['discount']>0){
                  $data[$i]['discount_price']=$data[$i]['price']-($data[$i]['price']*($data[$i]['discount']/100));
                  }
                 $k++;}                  
                  }

                  $ress[$j][$menu['name']]=$data;
                  $re[$j]=$k-1;
                  $res['result'][$j]=$ress[$j];
                  }
            $j++;}
            $res['count']=implode(',',$re);
          }
            $this->set_response_simple(($res == FALSE)? FALSE : $res, 'Success..!', REST_Controller::HTTP_OK, TRUE);
            /*$this->set_response_simple(($list == FALSE)? FALSE : $list, 'Success..!', REST_Controller::HTTP_OK, TRUE);*/
         }
       
       
   }




   /**
    * @author Mahesh
    * @desc To get Details of Single Item
    * @param string $item_id
    */
   public function food_item_get($item_id) {
        if(!empty($item_id)){
           $data = $this->food_item_model->fields('id,name,desc,price,discount,quantity,status,menu_id')->where('id', $item_id)->get();
           if(! empty($data)){
                   $data['image'] = base_url().'uploads/food_item_image/food_item_'.$data['id'].'.jpg';
                   $data['item_status']= ($data['status']==1)? 'Available' : 'Not Available' ;
                   if($data['discount']>0){
                   $data['discount_price']=$data['price']-($data['price']*($data['discount']/100));
                  }
           }
           $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
       }
   }
   /**
    * @author Mahesh
    * @desc To get list of Sections
    * @param string $target
    **/
   /*public function FoodSections_get($item_id) {
       if(!empty($item_id)){
           $data['result'] = $this->food_section_model->fields('id,name,required,item_field')->where('item_id', $item_id)->get_all();
           $data['item_filed_types']=['1'=>'Radion','2'=>'Checkbox'];
           $data['required_types']=['1'=>'Yes','0'=>'No'];
           $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
       }
   }*/
   public function FoodSections_get($item_id) {
       if(!empty($item_id)){
           $sec = $this->food_section_model->fields('id,name,required,item_field')->where('item_id', $item_id)->get_all();
           $sections=array();
           $s=0;
           foreach ($sec as $sc) {

              $sec_item = $this->food_sec_item_model->fields('id,name,desc,price,status')->where('sec_id', $sc['id'])->get_all();
              $section_items=array();
             if(! empty($sec_item)){
                 for ($i = 0; $i < count($sec_item) ; $i++){
                  $section_items[$i]=$sec_item[$i];
                     /*$section_items[$i]['image'] = base_url().'uploads/food_sec_item_image/food_sec_item_'.$sec_item[$i]['id'].'.jpg';*/
                     $section_items[$i]['sec_item_status']= ($sec_item[$i]['status']==1)? 'Available' : 'Not Available' ;
                 }
             }
             $sc['sec_items']=$section_items;
             $sections[]=$sc;
           }
           /*echo "<ul>";
           foreach ($sections as $row) {
            if(count($row['sec_items'])>0){
              if($row['item_field']==1){
                $input_type='radio';
              }elseif($row['item_field']==2){
                $input_type='checkbox';
              }
             echo "<li>".$row['name'];
             echo "<ul>";
                foreach ($row['sec_items'] as $sc_i) {
                  echo "<li><label><input type='".$input_type."' name='".$row['name']."[]'/>".$sc_i['name']."<span class='pull-right'> &nbsp;&nbsp;&nbsp;Price:".$sc_i['price']."</span></label></li>";  
                }
              echo "</ul>";
             echo "</li>";
           }
             echo "<br/><br/>";
           }
           echo "</ul>";
           die;*/
           $data['result'] =$sections;

           $data['item_filed_types']=['1'=>'Radion','2'=>'Checkbox'];
           $data['required_types']=['1'=>'Yes','0'=>'No'];
           $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
       }
   }
   /**
    * @author Mahesh
    * @desc To get list of Section Items
    * @param string $target
    */
   public function FoodSecItems_get($sec_id) {
       if(!empty($sec_id)){
           $data['result'] = $this->food_sec_item_model->fields('id,name,desc,price,status')->where('sec_id', $sec_id)->get_all();
           if(! empty($data)){
               for ($i = 0; $i < count($data['result']) ; $i++){
                   /*$data['result'][$i]['image'] = base_url().'uploads/food_sec_item_image/food_sec_item_'.$data['result'][$i]['id'].'.jpg';*/
                   $data['result'][$i]['sec_item_status']= ($data['result'][$i]['status']==1)? 'Available' : 'Not Available' ;
               }
           }
           $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
       }
   }
   /**
    * @author Mahesh
    * @desc To get Details of Section Items
    * @param string $target
    */
   public function FoodSecItem_get($sec_item_id) {
       if(!empty($sec_item_id)){
           $data = $this->food_sec_item_model->fields('id,name,desc,price,status')->where('id', $sec_item_id)->get();
           if(! empty($data)){
                   $data['image'] = base_url().'uploads/food_sec_item_image/food_sec_item_'.$data['id'].'.jpg';
                   $data['sec_item_status']= ($data['status']==1)? 'Available' : 'Not Available' ;
           }
           $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
       }
   }
   /**
    * @author Mahesh
    * @desc To Place an Order
    */
   /*public function OrderFood_POST()
   {
    $this->form_validation->set_rules($this->food_menu_model->rules);
    // Run Validations
    if ($this->form_validation->run() == FALSE) {
      $this->set_response_simple(validation_errors(), 'Failed..!', REST_Controller::HTTP_OK, TRUE);
    }else{
      $data=1;
      $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }

   }*/


   /**
     * Food Order
     *
     * @author Mahesh
     * @desc To Manage Food Order
     * @param string
     */
    public function FoodOrder_POST(){
            $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
            $_POST = json_decode(file_get_contents("php://input"), TRUE);
            //$this->form_validation->set_rules($this->users_address_model->rules);
            $this->form_validation->set_rules($this->food_orders_model->rules);
            if ($this->form_validation->run() == false) {
                $this->set_response_simple(validation_errors(), 'Validation Error', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
                /*$address_id = '';
                if(empty($_POST['address_id'])){
                    $address_id = $this->users_address_model->insert([
                        'user_id' => $token_data->id,
                        'name' => $this->input->post('name'),
                        'email' => $this->input->post('email'),
                        'phone' => $this->input->post('mobile'),
                        'address' => $this->input->post('address'),
                    ]);
                }else {
                    $address_id = $_POST['address_id'];
                }*/
                $order_id = $this->food_orders_model->insert([
                    'user_id' => $token_data->id,
                    'order_track' => rand(),
                    'discount' => $this->input->post('discount'),
                    'delivery_fee' => $this->input->post('delivery_fee'),
                    'tax' => $this->input->post('tax'),
                    'total' => $this->input->post('total'),
                    'coupon_id' => $this->input->post('coupon_id'),
                    'address_id' => $this->input->post('address_id'),
                    'delivery' => $this->input->post('delivery'),
                    'payment_method_id' => $this->input->post('payment_method_id'),
                    'instructions' => $this->input->post('instructions'),
                    'vendor_id' => $this->input->post('vendor_id'),
                ]);
                if(!empty($order_id)){
                    foreach ($_POST['items'] as $item){
                      $sec_it=NULL;
                      if(!empty($item['sec_item_id'])){
                        $sec_it=$item['sec_item_id'];
                      }
                        $item_id = $this->food_order_items_model->insert([
                            'order_id' => $order_id,
                            'item_id' => $item['item_id'],
                            'quantity' => $item['quantity'],
                            'price' => $item['price'],
                            'sec_item_id' => $sec_it,
                        ]);
                    }
                    if(!empty($_POST['sec_items'])){
                    foreach ($_POST['sec_items'] as $sub_item){
                        $sub_id = $this->food_sub_order_items_model->insert([
                            'order_id' => $order_id,
                            'sec_item_id' => $sub_item['sec_item_id'],
                            'quantity' => $sub_item['sec_quantity'],
                            'price' => $sub_item['sec_price'],
                        ]);
                    }
                  }
                }
                $this->set_response_simple($order_id, 'Success..!', REST_Controller::HTTP_CREATED, TRUE);
            }
    }

    /**
     * Food Order
     *
     * @author Mahesh
     * @desc To Manage Food Orders
     * @param string
     */
    public function FoodOrders_get($type,$user_id){
      if(!empty($type) && !empty($user_id)){
        if($type=='past'){
          $this->db->where('order_status',6);
        }elseif($type=='upcoming'){
          $this->db->where('order_status !=',0);
          $this->db->where('order_status !=',6);
          $this->db->where('order_status !=',7);
        }
       $data = $this->food_orders_model->with_order_items('fields:item_id,price,quantity')->with_sub_order_items('fields:sec_item_id,price,quantity')->fields('id,discount,delivery_fee,tax,total,deal_id,order_track,order_status')->where('user_id',$user_id)->order_by('id', 'DESC')->get_all();

           if(! empty($data)){
                 $status=['0'=>'Rejected','1'=>'Order Received','2'=>'Accepted','3'=>'Preparing','4'=>'Out for delivery','5'=>'Order on the way','6'=>'Order Completed','7'=>'Cancelled'];
                for ($i = 0; $i < count($data) ; $i++){
                    $data[$i]['order_status']=$status[$data[$i]['order_status']];
            }
           
            }
            /*$data['order_status']=['0'=>'Rejected','1'=>'Order Recived','2'=>'Accepted','3'=>'Preparing','4'=>'Out for delivery','5'=>'Order on the way','6'=>'Order Completed'];*/
           $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
      }
    }
     /**
    * @author Mahesh
    * @desc To Accept the requested orders
    */

  /* public function FoodOrderReviews_POST()
   {
 $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        $input=$this->post();
        $address_id = $this->users_address_model->insert([
                        'user_id' => $token_data->id,
                        'name' => $this->input->post('name'),
                        'email' => $this->input->post('email'),
                        'phone' => $this->input->post('mobile'),
                        'address' => $this->input->post('address'),
                    ]);
        $this->set_response_simple(($r == FALSE)? FALSE : $r, 'Success..!', REST_Controller::HTTP_OK, TRUE);
       
   }*/







     /* Food Delivery Boy App Start */
    /**
    * @author Mahesh
    * @desc To get list of All Past Delivery Orders
    */
   public function FoodDealOldHistory_get(){
    $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
           $data = $this->food_order_deal_model->with_order('fields:vendor_id,user_id,address_id,order_track,order_status,payment_method_id,total')->with_deal_boy('fields: id, first_name')->fields('id,order_id,deal_id,otp')->where('deal_id', $token_data->id)->where('ord_deal_status', 2)->order_by('id','DESC')->get_all();
           $result=array();
           if(!empty($data)){
            $i=0;foreach ($data as $row) {
              if($row['order']['order_status']==6){
              $v=$this->vendor_list_model->where('id',$row['order']['vendor_id'])->get();
              $u=$this->users_address_model->where('id',$row['order']['address_id'])->get();
              $result[$i]['order_number']=$row['order']['order_track'];
              $result[$i]['resturant']=$v['name'];
              $result[$i]['resturant_address']=$v['address'];
              $result[$i]['customer']=$u['name'];
                
              if($row['order']['payment_method_id']==1){
                $result[$i]['price']=$row['order']['total'];  
              }

            $i++;}

              }
           }

           $this->set_response_simple(($result == FALSE)? FALSE : $result, 'Success..!', REST_Controller::HTTP_OK, TRUE);
   }
    /**
    * @author Mahesh
    * @desc To get list of All Notifications of Orders from Resturant
    * @param string $ord_deal_status
    */
   public function FoodDealOrdRequests_get($ord_deal_status) {
    $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
           $data = $this->food_order_deal_model->with_order('fields:vendor_id,user_id,address_id,order_status,payment_method_id,total,order_track')->with_deal_boy('fields: id, first_name')->fields('id,order_id,deal_id,otp')->where('deal_id', $token_data->id)->where('ord_deal_status', $ord_deal_status)->order_by('id','DESC')->get_all();
           $result=array();
           if(!empty($data)){
            $i=0;foreach ($data as $row) {
              if($row['order']['order_status']==2 || $row['order']['order_status']==3 || $row['order']['order_status']==4 || $row['order']['order_status']==5){
              $v=$this->vendor_list_model->with_location('fields:latitude,longitude')->where('id',$row['order']['vendor_id'])->get();
              $u=$this->users_address_model->with_location('fields:latitude,longitude')->where('id',$row['order']['address_id'])->get();
              /*print_r($v);die;*/
              $result[$i]['id']=$row['id'];
              $result[$i]['order_id']=$row['order_id'];
              $result[$i]['order_no']=$row['order']['order_track'];
              $result[$i]['resturant']=$v['name'];
              $result[$i]['resturant_address']=$v['address'];
              $result[$i]['resturant_lat']=$v['location']['latitude'];
              $result[$i]['resturant_lng']=$v['location']['longitude'];
              $result[$i]['customer']=$u['name'];
              $result[$i]['customer_address']=$u['address'];
              $result[$i]['customer_lat']=$u['location']['latitude'];
              $result[$i]['customer_lng']=$u['location']['longitude'];
              if($ord_deal_status == 2){
                $result[$i]['otp']=$row['otp'];
              }
              if($row['order']['order_status'] == 4){
                    $result[$i]['orderstatus']='Order Received';
                    $result[$i]['orderstatus_id']=5;
                }

                if($row['order']['order_status'] == 5){
                    if($row['order']['payment_method_id']==1){
                      $result[$i]['price']=$row['order']['total'];  
                    }
                    $result[$i]['customer_number']=$u['phone'];
                    $result[$i]['orderstatus']='Order Completed';
                    $result[$i]['orderstatus_id']=6;
                }
            $i++;}

              }
           }

           $this->set_response_simple(($result == FALSE)? FALSE : $result, 'Success..!', REST_Controller::HTTP_OK, TRUE);
   }

 /**
    * @author Mahesh
    * @desc To Accept the requested orders
    */

   public function FoodDealOrdRequest_POST()
   {
 $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        $input=$this->post();
         $v=$this->food_order_deal_model->where('id',$input['id'])->get();
         if($input['status'] == 2){
        $ord_id =  $this->food_order_deal_model->update([
                        'id' => $input['id'],
                        'ord_deal_status' => $input['status'],
                        'otp' =>rand(1234,9567)
                    ], 'id');
        if($ord_id){
          $r=$this->food_order_deal_model->update([
                        'deleted_at' =>date('Y-m-d H:i:s')
                    ], [
                    'id !='=>$input['id'],
                    'order_id'=>$v['order_id'],
                    'ord_deal_status'=>1
                  ]);
        }
      }elseif($input['status'] == ''){
        $r =  $this->food_order_deal_model->update([
                        'id' => $input['id'],
                        'deleted_at' =>date('Y-m-d H:i:s')
                    ], 'id');
      }

        $this->set_response_simple(($r == FALSE)? FALSE : $r, 'Success..!', REST_Controller::HTTP_OK, TRUE);
       
   }
    /**
    * @author Mahesh
    * @desc To Update Received order Status
    */
   public function FoodDealOrdReceived_POST()
   {
 $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        $input=$this->post();
         $v=$this->food_order_deal_model->where('id',$input['id'])->get();
        $ord_id =  $this->food_orders_model->update([
                        'id' => $v['order_id'],
                        'order_status' => $input['orderstatus_id']
                    ], 'id');
        $this->set_response_simple(($ord_id == FALSE)? FALSE : $ord_id, 'Success..!', REST_Controller::HTTP_OK, TRUE);
       
   }
     /**
    * @author Mahesh
    * @desc To Update Delivery Boy Status
    */
   public function DeliveryBoyStatus_POST()
   {
  $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        $input=$this->post();
        $v=$this->delivery_boy_status_model->where('user_id',$token_data->id)->get();
                    if($v != ''){
                    $id_deal=$this->delivery_boy_status_model->update([
                        'delivery_boy_status' => $this->input->post('delivery_boy_status')
                    ], ['user_id',$token_data->id]);
                }else{
                    $id_deal=$this->delivery_boy_status_model->insert([
                        'delivery_boy_status' => $this->input->post('delivery_boy_status'),
                        'user_id'=>$token_data->id
                    ]);
                }
        $this->set_response_simple(($id_deal == FALSE)? FALSE : $id_deal, 'Success..!', REST_Controller::HTTP_OK, TRUE);
   }
    /* Food Delivery Boy App End */






















    /**
    * @author Mahesh
    * @desc To get list of Food Cart Based on vendor and user id
    * @param string $vendor_id
    * @param string $user_id 
    */
  /* public function FoodCart_get($user_id) {
    if(!empty($user_id)){
           $data = $this->food_cart_model->fields('id,item_id,quantity')->where('user_id', $user_id)->get_all();
           $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
         }
   }*/
   /**
    * @author Mahesh
    * @ POST Add Cart
    */
 /*  public function FoodCart_POST()
   {

        $input=$this->post();
        for ($i=0; $i < count($input['item_id']); $i++) {
         if(empty($input['id'])){
        $cart_id = $this->food_cart_model->insert([
                        'user_id' => $input['user_id'],
                        'item_id' => $input['item_id'][$i],
                        'quantity' => $input['quantity'][$i]
                    ]);
          }else{
           $cart_id =  $this->food_menu_model->update([
                        'id' => $input['id'],
                        'quantity' => $input['quantity'][$i]
                    ], 'id');
          }
        }
        $this->set_response_simple(($cart_id == FALSE)? FALSE : $cart_id, 'Success..!', REST_Controller::HTTP_OK, TRUE);
       
   }*/







   /*public function user_details_get($u_type='')
    {
      $this->load->model('user_model');
      $this->load->model('group_model');
        $result=$this->user_model->order_by('id', 'DESC')->fields('id,unique_id,first_name,last_name,email,phone')->with_groups('fields:name,id')->get_all();
      $this->set_response_simple(($result == FALSE)? FALSE : $result, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }*/

}