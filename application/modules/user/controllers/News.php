<?php
require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';
use Firebase\JWT\JWT;
class News extends MY_REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('news_model');
        $this->load->model('news_category_model');
    }
    
    
    /**
     * @author Mehar
     * @desc To get list of news categories and targeted category as well
     * @param string $target
     */
    public function news_categories_get($target = '') {
        if(empty($target)){
            $data = $this->news_category_model->fields('id, name')->get_all();
            if(! empty($data)){
                for ($i = 0; $i < count($data) ; $i++){
                    $data[$i]['image'] = base_url().'uploads/news_category_image/news_category_'.$data[$i]['id'].'.jpg';
                }
            }
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }else{
            $data = $this->news_category_model->fields('id, name')->where('id', $target)->get();
            $data['image'] = base_url().'uploads/news_category_image/news_category_'.$target.'.jpg';
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    
    
    /**
     * To get the news
     *
     * @author Mehar
     * @param number $limit,offset
     */
    public function news_get($limit = 10, $offset = 0)
    {   $news_id = $this->input->get('news_id');
        if(! isset($news_id)){
            $data = $this->news_model->all($limit, $offset, $this->input->get('cat_id'));
            if (! empty($data['result'])) {
                foreach ($data['result'] as $d) {
                    $d->image = base_url() . 'uploads/news_image/news_' . $d->id . '.jpg';
                }
            }
            $this->set_response_simple((empty($data['result'])) ? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }else{
            $data = $this->news_model->where('id', $news_id)->get();
            $this->set_response_simple((empty($data)) ? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
        
    }
}
