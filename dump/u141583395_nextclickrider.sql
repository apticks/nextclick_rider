-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 21, 2020 at 07:18 AM
-- Server version: 10.2.31-MariaDB
-- PHP Version: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u141583395_nextclickrider`
--

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `code` varchar(10) NOT NULL,
  `last_id` bigint(20) NOT NULL DEFAULT 100,
  `priority` smallint(6) NOT NULL,
  `terms` text NOT NULL,
  `privacy` text NOT NULL,
  `created_user_id` bigint(20) DEFAULT NULL,
  `updated_user_id` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0 = deleted, 1 = Active, 2 = Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`, `code`, `last_id`, `priority`, `terms`, `privacy`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`, `deleted_at`, `status`) VALUES
(1, 'admin', 'Admin', 'NCA', 101, 1, '<p>hgggfghg</p>\r\n', '', NULL, 1, '0000-00-00 00:00:00', '2019-12-13 10:53:37', NULL, 1),
(2, 'executive', 'Executive', 'NCE', 137, 2, '', '', NULL, 1, '0000-00-00 00:00:00', '2019-12-04 13:50:57', NULL, 1),
(3, 'mehars', 'sdfa', 'lp_s2', 100, 10, '', '', NULL, 1, '2019-09-23 19:43:51', '2019-10-04 09:16:09', '2019-10-05 15:02:22', 1),
(4, 'mehars', 'sdfa', 'lp_s2', 101, 10, '', '', NULL, 1, '2019-09-25 19:18:22', '2019-10-03 16:25:09', '2019-10-03 16:57:23', 1),
(5, 'mehars', 'sdfa', 'lp_s2', 104, 10, '', '', NULL, 1, '2019-09-25 19:18:57', '2019-10-03 16:25:09', '2019-10-03 16:57:17', 1),
(6, 'mehars', 'sdfa', 'lp_s2', 101, 10, '', '', NULL, 1, '2019-09-25 19:19:34', '2019-10-03 16:25:09', '2019-10-03 16:57:11', 1),
(7, 'mehars', 'sdfa', 'lp_s2', 100, 10, '', '', NULL, 1, '2019-09-25 19:19:52', '2019-10-03 16:25:09', '2019-10-03 16:57:06', 1),
(8, 'mehars', 'sdfa', 'lp_s2', 100, 10, '', '', NULL, 1, '2019-09-25 19:20:04', '2019-10-03 16:25:09', '2019-10-03 16:57:00', 1),
(9, 'mehars', 'sdfa', 'lp_s2', 100, 10, '', '', NULL, 1, '2019-09-25 19:20:22', '2019-10-03 16:25:09', '2019-10-03 16:56:54', 1),
(10, 'mehars', 'sdfa', 'lp_s2ss', 0, 10, '', '', 1, 1, '2019-10-03 15:59:56', '2019-10-03 16:25:37', '2019-10-03 16:38:52', 1),
(11, 'mehars', 'sdfa', 'lp_s2', 0, 10, '', '', 1, 1, '2019-10-03 16:17:40', '2019-10-03 16:25:09', '2019-10-03 16:39:19', 1),
(12, 'Apple', 'sdfsd', 'NCAP', 0, 4, '', '', 1, 1, '2019-10-04 09:23:21', '2019-10-04 09:23:59', '2019-10-05 15:02:29', 1),
(13, 'Samsung', '123', 'lp_Mea', 0, 10, '', '', 1, 1, '2019-10-04 09:28:37', '2019-10-04 09:28:50', '2019-10-05 15:02:36', 1),
(14, 'vendor', 'test', 'tp12', 139, 1, '', '', 1, NULL, '2019-11-02 07:39:12', '2019-11-07 11:52:31', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `groups_permissions`
--

CREATE TABLE `groups_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `perm_id` int(11) NOT NULL,
  `value` tinyint(4) DEFAULT 0 COMMENT '0 = Deny, 1 = Allow',
  `created_user_id` bigint(20) DEFAULT NULL,
  `updated_user_id` bigint(20) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups_permissions`
--

INSERT INTO `groups_permissions` (`id`, `group_id`, `perm_id`, `value`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(6, 4, 1, 1, NULL, NULL, 1569419646, 1569419646),
(7, 5, 1, 1, NULL, NULL, 1569420580, 1569420580),
(8, 10, 2, 1, NULL, NULL, 2019, 0),
(9, 10, 1, 1, 1, NULL, 2019, 0),
(10, 11, 1, 1, 1, NULL, 2019, 0),
(11, 11, 2, 1, 1, NULL, 2019, 0),
(13, 3, 1, 1, NULL, NULL, 1570173369, 1570173369),
(14, 3, 2, 1, NULL, NULL, 1570173369, 1570173369),
(18, 2, 2, 1, NULL, NULL, 1570173391, 1570173391),
(19, 12, 1, 1, NULL, NULL, 1570173839, 1570173839),
(20, 12, 2, 1, NULL, NULL, 1570173839, 1570173839),
(23, 13, 1, 1, NULL, NULL, 1570174130, 1570174130),
(157, 14, 17, 1, NULL, NULL, 1572676811, 1572676811),
(158, 14, 45, 1, NULL, NULL, 1572676811, 1572676811),
(159, 14, 9, 1, NULL, NULL, 1572676811, 1572676811),
(160, 14, 33, 1, NULL, NULL, 1572676811, 1572676811),
(161, 14, 29, 1, NULL, NULL, 1572676811, 1572676811),
(162, 14, 57, 1, NULL, NULL, 1572676811, 1572676811),
(163, 14, 1, 1, NULL, NULL, 1572676811, 1572676811),
(164, 14, 62, 1, NULL, NULL, 1572676811, 1572676811),
(165, 14, 49, 1, NULL, NULL, 1572676811, 1572676811),
(166, 14, 48, 1, NULL, NULL, 1572676811, 1572676811),
(167, 14, 5, 1, NULL, NULL, 1572676811, 1572676811),
(168, 14, 21, 1, NULL, NULL, 1572676811, 1572676811),
(169, 14, 41, 1, NULL, NULL, 1572676811, 1572676811),
(170, 14, 25, 1, NULL, NULL, 1572676811, 1572676811),
(171, 14, 13, 1, NULL, NULL, 1572676811, 1572676811),
(172, 14, 37, 1, NULL, NULL, 1572676811, 1572676811),
(174, 1, 45, 1, NULL, NULL, 1573121306, 1573121306),
(178, 1, 57, 1, NULL, NULL, 1573121307, 1573121307),
(180, 1, 62, 1, NULL, NULL, 1573121307, 1573121307),
(181, 1, 49, 1, NULL, NULL, 1573121307, 1573121307),
(182, 1, 48, 1, NULL, NULL, 1573121307, 1573121307),
(233, 1, 1, 1, NULL, NULL, 1576230817, 1576230817),
(234, 1, 27, 1, NULL, NULL, 1576230817, 1576230817),
(235, 1, 29, 1, NULL, NULL, 1576230817, 1576230817),
(236, 1, 28, 1, NULL, NULL, 1576230818, 1576230818),
(237, 1, 13, 1, NULL, NULL, 1576230818, 1576230818),
(238, 1, 10, 1, NULL, NULL, 1576230818, 1576230818),
(239, 1, 24, 1, NULL, NULL, 1576230818, 1576230818),
(240, 1, 12, 1, NULL, NULL, 1576230818, 1576230818),
(241, 1, 22, 1, NULL, NULL, 1576230818, 1576230818),
(242, 1, 23, 1, NULL, NULL, 1576230818, 1576230818),
(243, 1, 11, 1, NULL, NULL, 1576230818, 1576230818),
(244, 1, 5, 1, NULL, NULL, 1576230818, 1576230818),
(245, 1, 36, 1, NULL, NULL, 1576230818, 1576230818),
(246, 1, 35, 1, NULL, NULL, 1576230818, 1576230818),
(247, 1, 34, 1, NULL, NULL, 1576230818, 1576230818),
(248, 1, 33, 1, NULL, NULL, 1576230818, 1576230818),
(249, 1, 39, 1, NULL, NULL, 1576230818, 1576230818),
(250, 1, 40, 1, NULL, NULL, 1576230818, 1576230818),
(251, 1, 37, 1, NULL, NULL, 1576230818, 1576230818),
(252, 1, 38, 1, NULL, NULL, 1576230818, 1576230818),
(253, 1, 16, 1, NULL, NULL, 1576230818, 1576230818),
(254, 1, 14, 1, NULL, NULL, 1576230818, 1576230818),
(255, 1, 30, 1, NULL, NULL, 1576230818, 1576230818),
(256, 1, 32, 1, NULL, NULL, 1576230818, 1576230818),
(257, 1, 31, 1, NULL, NULL, 1576230818, 1576230818),
(258, 1, 15, 1, NULL, NULL, 1576230819, 1576230819),
(259, 1, 44, 1, NULL, NULL, 1576230819, 1576230819),
(260, 1, 19, 1, NULL, NULL, 1576230819, 1576230819),
(261, 1, 43, 1, NULL, NULL, 1576230819, 1576230819),
(262, 1, 26, 1, NULL, NULL, 1576230819, 1576230819),
(263, 1, 25, 1, NULL, NULL, 1576230819, 1576230819),
(264, 1, 4, 1, NULL, NULL, 1576230819, 1576230819),
(265, 1, 21, 1, NULL, NULL, 1576230819, 1576230819),
(266, 1, 9, 1, NULL, NULL, 1576230819, 1576230819),
(267, 1, 20, 1, NULL, NULL, 1576230819, 1576230819),
(268, 1, 2, 1, NULL, NULL, 1576230819, 1576230819),
(269, 1, 6, 1, NULL, NULL, 1576230819, 1576230819),
(270, 1, 7, 1, NULL, NULL, 1576230819, 1576230819),
(271, 1, 8, 1, NULL, NULL, 1576230819, 1576230819),
(272, 1, 18, 1, NULL, NULL, 1576230819, 1576230819),
(273, 1, 17, 1, NULL, NULL, 1576230819, 1576230819),
(274, 1, 42, 1, NULL, NULL, 1576230819, 1576230819),
(275, 1, 41, 1, NULL, NULL, 1576230819, 1576230819),
(276, 1, 3, 1, NULL, NULL, 1576230819, 1576230819);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `ip_address`, `login`, `time`) VALUES
(3, '::1', 'admin@admin.comd', 1570175659),
(4, '::1', 'admin@admin.comd', 1570175659),
(7, '::1', 'admin@admin.comm', 1570175712),
(8, '::1', 'admin@admin.comd', 1570175848),
(15, '::1', 'nac0101', 1570177804),
(16, '::1', 'nac0101', 1570177804),
(17, '::1', 'NAC0101', 1570177929),
(25, '::1', 'NAC0101', 1570183283),
(26, '::1', 'NAC0101', 1570183283),
(27, '::1', 'NAC0101', 1570183296);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `perm_key` varchar(30) NOT NULL,
  `perm_name` varchar(100) NOT NULL,
  `parent_status` varchar(100) DEFAULT 'parent',
  `description` longtext NOT NULL,
  `created_user_id` bigint(20) DEFAULT NULL,
  `updated_user_id` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0 = deleted, 1 = Active, 2 = Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `perm_key`, `perm_name`, `parent_status`, `description`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`, `deleted_at`, `status`) VALUES
(1, 'admin', 'Admin Module', 'parent', '', NULL, NULL, '2019-12-10 16:20:12', NULL, NULL, 1),
(2, 'withdrawal', 'Payment Withdrawal Management', '1', '', NULL, NULL, '2019-12-10 16:22:39', NULL, NULL, 1),
(3, 'vendors_list', 'Vendor Management', '1', '', NULL, NULL, '2019-12-10 16:23:32', NULL, NULL, 1),
(4, 'list_master', 'Listing Master Data Mangement', '1', '', NULL, NULL, '2019-12-10 16:23:58', NULL, NULL, 1),
(5, 'emp', 'Employee Mangement', '1', '', NULL, NULL, '2019-12-10 16:24:39', NULL, NULL, 1),
(6, 'role', 'Role Management', '1', '', NULL, NULL, '2019-12-10 16:24:54', NULL, NULL, 1),
(7, 'site_settings', 'Site Settings', '1', '', NULL, NULL, '2019-12-10 16:25:21', NULL, NULL, 1),
(8, 'slider_settings', 'Slider Settings', '1', '', NULL, NULL, '2019-12-10 16:25:41', NULL, NULL, 1),
(9, 'news_categories', 'News Category Mangement', '1', '', NULL, NULL, '2019-12-10 16:26:06', NULL, NULL, 1),
(10, 'ecom_category', 'Ecom Category Management', '1', '', NULL, NULL, '2019-12-10 16:26:46', NULL, NULL, 1),
(11, 'ecom_sub_category', 'Ecommerce Sub Category Management', '1', '', NULL, NULL, '2019-12-10 16:27:35', NULL, NULL, 1),
(12, 'ecom_brand', 'Ecommerce Brand Management', '1', '', NULL, NULL, '2019-12-10 16:27:54', NULL, NULL, 1),
(13, 'doctor_specialisation', 'Doctor Specialization Management', '1', '', NULL, NULL, '2019-12-10 16:28:51', NULL, NULL, 1),
(14, 'groceries_category', 'Groceries Category Management', '1', '', NULL, NULL, '2019-12-10 16:31:19', NULL, NULL, 1),
(15, 'groceries_sub_category', 'Groceries Sub Category Management', '1', '', NULL, NULL, '2019-12-10 16:31:46', NULL, NULL, 1),
(16, 'groceries_brand', 'Groceries Brand Management', '1', '', NULL, NULL, '2019-12-10 16:32:11', NULL, NULL, 1),
(17, 'vehicle_brand', 'Travel Vehicle Brand Management', '1', '', NULL, NULL, '2019-12-10 16:34:36', NULL, NULL, 1),
(18, 'travel_accessory', 'Travel Accessories Management', '1', '', NULL, NULL, '2019-12-10 16:35:24', NULL, NULL, 1),
(19, 'service_type', 'Home Service Type Management', '1', '', NULL, NULL, '2019-12-10 16:35:51', NULL, NULL, 1),
(20, 'news', 'News Module', 'parent', '', NULL, NULL, '2019-12-10 16:37:36', NULL, NULL, 1),
(21, 'manage_news', 'Manage News', '20', '', NULL, NULL, '2019-12-10 16:38:41', NULL, NULL, 1),
(22, 'ecom', 'Ecommerce Module', 'parent', '', NULL, NULL, '2019-12-10 16:39:10', NULL, NULL, 1),
(23, 'ecom_product', 'Ecommerce Product Management', '22', '', NULL, NULL, '2019-12-10 16:39:40', NULL, NULL, 1),
(24, 'ecom_orders', 'Ecom Orders Management', '22', '', NULL, NULL, '2019-12-10 16:40:00', NULL, NULL, 1),
(25, 'hospital', 'Hospital Module', 'parent', '', NULL, NULL, '2019-12-10 16:40:23', NULL, NULL, 1),
(26, 'hosp_doctor', 'Hospital Doctor Management', '25', '', NULL, NULL, '2019-12-10 16:40:55', NULL, NULL, 1),
(27, 'beauty', 'Beauty Module', 'parent', '', NULL, NULL, '2019-12-10 16:41:10', NULL, NULL, 1),
(28, 'beauty_package', 'Beauty Package Management', '27', '', NULL, NULL, '2019-12-10 16:41:39', NULL, NULL, 1),
(29, 'beauty_orders', 'Beauty Order Management', '27', '', NULL, NULL, '2019-12-10 16:42:08', NULL, NULL, 1),
(30, 'grocery', 'Groceries Module', 'parent', '', NULL, NULL, '2019-12-10 16:42:37', NULL, NULL, 1),
(31, 'grocery_product', 'Groceries Product Management', '30', '', NULL, NULL, '2019-12-10 16:43:04', NULL, NULL, 1),
(32, 'grocery_orders', 'Groceries Orders Management', '30', '', NULL, NULL, '2019-12-10 16:43:25', NULL, NULL, 1),
(33, 'food', 'Food Module', 'parent', '', NULL, NULL, '2019-12-10 16:43:42', NULL, NULL, 1),
(34, 'food_menu', 'Food Menu Management', '33', '', NULL, NULL, '2019-12-10 16:44:06', NULL, NULL, 1),
(35, 'food_items', 'Food Items Management', '33', '', NULL, NULL, '2019-12-10 16:44:25', NULL, NULL, 1),
(36, 'food_extra_sections', 'Food Extra Section Management', '33', '', NULL, NULL, '2019-12-10 16:44:56', NULL, NULL, 1),
(37, 'food_section_items', 'Food Section Items Management', '33', '', NULL, NULL, '2019-12-10 16:45:27', NULL, NULL, 1),
(38, 'food_settings', 'Food Settings', '33', '', NULL, NULL, '2019-12-10 16:46:02', NULL, NULL, 1),
(39, 'food_orders', 'Food Orders Management', '33', '', NULL, NULL, '2019-12-10 16:46:24', NULL, NULL, 1),
(40, 'food_reports', 'Food Reports', '33', '', NULL, NULL, '2019-12-10 16:46:46', NULL, NULL, 1),
(41, 'travels', 'Travels Module', 'parent', '', NULL, NULL, '2019-12-10 16:47:04', NULL, NULL, 1),
(42, 'travel_vehicle', 'Travel Vehicle Management', '41', '', NULL, NULL, '2019-12-10 16:47:30', NULL, NULL, 1),
(43, 'home_services', 'Home Services Module', 'parent', '', NULL, NULL, '2019-12-10 18:07:47', NULL, NULL, 1),
(44, 'service_person', 'Home Service Person Management', '43', '', NULL, NULL, '2019-12-10 18:09:03', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `riders`
--

CREATE TABLE `riders` (
  `id` bigint(20) NOT NULL,
  `own_vehicle` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0 = No, 1=Yes',
  `full_name` varchar(50) NOT NULL,
  `father_or_husband` varchar(50) NOT NULL,
  `perm_add` varchar(50) NOT NULL,
  `current_add` varchar(50) NOT NULL,
  `zipcode` int(20) NOT NULL,
  `state` varchar(25) NOT NULL,
  `district_or_city` varchar(25) NOT NULL,
  `constituency` varchar(25) NOT NULL,
  `dob` date NOT NULL,
  `gender` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=male,1=female',
  `lang` varchar(50) NOT NULL,
  `marital_status` tinyint(4) DEFAULT 0 COMMENT '0=unmarried,1=married',
  `qualification` varchar(25) NOT NULL,
  `email` varchar(25) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `mobile_1` bigint(20) NOT NULL,
  `whatsapp_num` varchar(15) NOT NULL,
  `adhaar_num` bigint(20) NOT NULL,
  `vehicle_reg_num` bigint(20) NOT NULL,
  `vehicle_reg_date` date NOT NULL,
  `driving_licen_no` bigint(20) NOT NULL,
  `driving_issue_date` date NOT NULL,
  `license_exp_date` date NOT NULL,
  `pollution_exp_date` date NOT NULL,
  `insurance_num` bigint(20) NOT NULL,
  `insurance_date` date NOT NULL,
  `insurance_exp_date` date NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0=deleted,1=Active,2=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `riders`
--

INSERT INTO `riders` (`id`, `own_vehicle`, `full_name`, `father_or_husband`, `perm_add`, `current_add`, `zipcode`, `state`, `district_or_city`, `constituency`, `dob`, `gender`, `lang`, `marital_status`, `qualification`, `email`, `mobile`, `mobile_1`, `whatsapp_num`, `adhaar_num`, `vehicle_reg_num`, `vehicle_reg_date`, `driving_licen_no`, `driving_issue_date`, `license_exp_date`, `pollution_exp_date`, `insurance_num`, `insurance_date`, `insurance_exp_date`, `created_at`, `updated_at`, `deleted_at`, `status`) VALUES
(1, 1, 'mehar koti trinadh babu', 'nagesh', 'palakollu', 'hyderabad', 500081, 'state 1', 'city 1', 'const 1', '1995-01-06', 0, 'english, telugu', 0, 'btech', 'mehargrepthor@gmail.com', '7013907291', 0, '7013907291', 0, 0, '2014-02-03', 7013907291, '2019-02-05', '2050-02-05', '2050-02-05', 51651496465614, '2019-02-03', '2050-02-03', '2020-01-03 14:39:24', NULL, NULL, 2),
(2, 1, 'mehar koti trinadh babu', 'nagesh', 'palakollu', 'hyderabad', 500081, 'state 1', 'city 1', 'const 1', '1995-01-06', 0, 'english, telugu', 0, 'btech', 'mehargrepthor@gmail.com', '7013907291', 0, '7013907291', 0, 0, '2014-02-03', 7013907291, '2019-02-05', '2050-02-05', '2050-02-05', 51651496465614, '2019-02-03', '2050-02-03', '2020-01-03 14:39:47', NULL, NULL, 2),
(3, 1, 'mehar koti trinadh babu', 'nagesh', 'palakollu', 'hyderabad', 500081, 'state 1', 'city 1', 'const 1', '1995-01-06', 0, 'english, telugu', 0, 'btech', 'mehargrepthor45@gmail.com', '7013907291', 7013907231, '7013907291', 132575862356, 13257586, '2014-02-03', 7013907291, '2019-02-05', '2050-02-05', '2050-02-05', 51651496465614, '2019-02-03', '2050-02-03', '2020-01-08 15:11:08', NULL, NULL, 2),
(4, 1, 'mehar koti trinadh babu', 'nagesh', 'palakollu', 'hyderabad', 500081, 'state 1', 'city 1', 'const 1', '1995-01-06', 0, 'english, telugu', 0, 'btech', 'mehargrepthor45@gmail.com', '7013907291', 7013907231, '7013907291', 132575862356, 13257586, '2014-02-03', 7013907291, '2019-02-05', '2050-02-05', '2050-02-05', 51651496465614, '2019-02-03', '2050-02-03', '2020-01-08 15:13:53', NULL, NULL, 2),
(5, 1, 'trupti', 'nagesh', 'palakollu', 'hyderabad', 500081, 'state 1', 'city 1', 'const 1', '1995-01-06', 0, 'english, telugu', 0, 'btech', 'tptrupti@gmail.com', '7013907291', 7013907231, '7013907291', 132575862356, 13257586, '2014-02-03', 7013907291, '2019-02-05', '2050-02-05', '2050-02-05', 51651496465614, '2019-02-03', '2050-02-03', '2020-01-09 09:41:31', NULL, NULL, 2),
(6, 1, 'test', 'nagesh', 'palakollu', 'hyderabad', 500081, 'state 1', 'city 1', 'const 1', '1995-01-06', 0, 'english, telugu', 0, 'btech', 'tptrupti@gmail.com', '7013907291', 7013907231, '7013907291', 132575862356, 13257586, '2014-02-03', 7013907291, '2019-02-05', '2050-02-05', '2050-02-05', 51651496465614, '2019-02-03', '2050-02-03', '2020-01-09 10:09:38', NULL, NULL, 2),
(7, 1, 'test', 'nagesh', 'palakollu', 'hyderabad', 500081, 'state 1', 'city 1', 'const 1', '1995-01-06', 0, 'english, telugu', 0, 'btech', 'tptrupti@gmail.com', '7013907291', 7013907231, '7013907291', 132575862356, 13257586, '2014-02-03', 7013907291, '2019-02-05', '2050-02-05', '2050-02-05', 51651496465614, '2019-02-03', '2050-02-03', '2020-01-09 10:11:20', NULL, NULL, 2),
(8, 1, 'trupti', 'nagesh', 'palakollu', 'hyderabad', 500081, 'state 1', 'city 1', 'const 1', '1995-01-06', 0, 'english, telugu', 0, 'btech', 'tptrupti@gmail.com', '7013907291', 7013907231, '7013907291', 132575862356, 13257586, '2014-02-03', 7013907291, '2019-02-05', '2050-02-05', '2050-02-05', 51651496465614, '2019-02-03', '2050-02-03', '2020-02-04 10:24:18', NULL, NULL, 2),
(9, 1, 'trupti', 'nagesh', 'palakollu', 'hyderabad', 500081, 'state 1', 'city 1', 'const 1', '1995-01-06', 0, 'english, telugu', 0, 'btech', 'tptrupti@gmail.com', '7013907291', 7013907231, '7013907291', 132575862356, 13257586, '2014-02-03', 7013907291, '2019-02-05', '2050-02-05', '2050-02-05', 51651496465614, '2019-02-03', '2050-02-03', '2020-02-07 13:09:35', NULL, NULL, 2),
(10, 1, 'trupti', 'nagesh', 'palakollu', 'hyderabad', 500081, 'state 1', 'city 1', 'const 1', '1995-01-06', 0, 'english, telugu', 0, 'btech', 'tptrupti@gmail.com', '7013907291', 7013907231, '7013907291', 132575862356, 13257586, '2014-02-03', 7013907291, '2019-02-05', '2050-02-05', '2050-02-05', 51651496465614, '2019-02-03', '2050-02-03', '2020-02-07 13:09:43', NULL, NULL, 2),
(11, 1, 'trupti', 'nagesh', 'palakollu', 'hyderabad', 500081, 'state 1', 'city 1', 'const 1', '1995-01-06', 0, 'english, telugu', 0, 'btech', 'tptrupti@gmail.com', '7013907291', 7013907231, '7013907291', 132575862356, 13257586, '2014-02-03', 7013907291, '2019-02-05', '2050-02-05', '2050-02-05', 51651496465614, '2019-02-03', '2050-02-03', '2020-02-07 13:22:11', NULL, NULL, 2),
(12, 1, 'trupti', 'nagesh', 'palakollu', 'hyderabad', 500081, 'state 1', 'city 1', 'const 1', '1995-01-06', 0, 'english, telugu', 0, 'btech', 'tptrupti@gmail.com', '7013907291', 7013907231, '7013907291', 132575862356, 13257586, '2014-02-03', 7013907291, '2019-02-05', '2050-02-05', '2050-02-05', 51651496465614, '2019-02-03', '2050-02-03', '2020-02-07 13:22:56', NULL, NULL, 2),
(13, 0, 'shshhsh', 'shshhsbbs', 'hshhshhs', 'hshshshhshdhj', 500081, 'Telangana', 'Hyderabad', 'Sirpur', '2020-02-07', 0, ' Telugu, Hindi, English,svhshhshs', 0, 'sgshhshs', 'gsgsg@ggg.njj', '4664949649', 6469464966, '9464664949', 794649649466, 0, '0000-00-00', 0, '2016-02-07', '2020-02-07', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-02-07 13:27:53', NULL, NULL, 2),
(14, 0, 'maniyar', 'maniyar', 'Hyderabad', 'hyderabad', 500081, 'Telangana', 'Hyderabad', 'Bellampalli', '2020-02-07', 0, ' Telugu, Hindi, English,', 0, 'b.tech', 'salavuddin@kwikytech.com', '7036375558', 7036375558, '7036375558', 546946467976, 0, '0000-00-00', 0, '2020-02-07', '2020-02-07', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-02-07 13:43:22', NULL, NULL, 2),
(15, 1, 'salavuddin', 'maniyar', 'hyderabad', 'hyderabad', 500081, 'Telangana', 'Hyderabad', 'Nirmal', '2020-02-07', 0, ' Telugu, Hindi, English,', 0, 'b.tech', 'salavuddin@kwikytech.com', '7036375558', 7036375558, '7036375558', 864649644946, 0, '2020-02-17', 0, '2020-02-17', '2020-02-17', '2020-02-17', 0, '2020-02-17', '2020-02-17', '2020-02-07 13:51:57', NULL, NULL, 2),
(16, 1, 'salavuddin maniyar', 'salavuddin maniyar', 'hyderabad', 'hyderabad', 500081, 'Telangana', 'Hyderabad', 'Mancherial', '2020-02-07', 0, ' Telugu, Hindi, English,', 0, 'b.tech', 'salavuddin.grepthor@gmail', '7036375558', 7036375558, '7036375558', 854576494649, 0, '2020-02-07', 0, '2020-02-07', '2020-02-07', '2020-02-07', 7728822, '2020-02-07', '2020-02-07', '2020-02-07 14:18:45', NULL, NULL, 2),
(17, 1, 'Sai Krishna', 'shanker', '10,133', 'Hyderabad', 504293, 'Telangana', 'Adilabad', 'Nirmal', '2020-02-08', 0, ' Hindi, English,', 0, 'b.tech', 'saisubba@gmail.com', '8977422783', 8919195804, '8977422783', 85235, 85239, '2020-02-12', 85238, '0000-00-00', '2020-02-12', '2020-02-21', 1235, '2020-02-11', '2020-02-18', '2020-02-08 06:05:53', NULL, NULL, 2),
(18, 1, 'sirikonda madhu', 'malliah', 'H no:2-55,Narsinghpur,dharmaram mandal,peddapalli ', 'Vidya Nagar,karimnagar', 505001, 'Telangana', 'Karim Nagar', 'Karimnagar', '0000-00-00', 0, ' Telugu, English,', 0, 'degree', 'madhuinphot@gmail.com', '9177358326', 9177358326, '9177358326', 554346922650, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-02-10 07:08:23', NULL, NULL, 2),
(19, 1, 'trupti', 'abc', 'palakollu', 'hyderabad', 500081, 'state 1', 'city 1', 'const 1', '1995-01-06', 1, 'english, telugu', 1, 'btech', 'tptrupti@gmail.com', '7013907291', 7013907231, '7013907291', 132575862356, 13257586, '2014-02-03', 7013907291, '2019-02-05', '2050-02-05', '2050-02-05', 51651496465614, '2019-02-03', '2050-02-03', '2020-02-10 11:30:46', NULL, NULL, 2),
(20, 1, 'Manikanta', 'Rajkumar', 'ndnndndndnndndnxnxnvdbbs', 'bdbnmnzgvvdvdbbdbbdbd', 500072, 'Telangana', 'Hyderabad', 'Khanapur', '2007-02-23', 0, ' Telugu, Hindi, English,', 1, 'btech', 'manikanta.grepthor@gmail.', '9635245555', 9654175774, '9684848548', 965477888959, 0, '1994-02-23', 0, '1998-02-26', '2022-02-27', '1998-02-26', 964949849, '2002-02-21', '2023-02-22', '2020-02-10 11:33:46', NULL, NULL, 2),
(21, 1, 'Birru Rajashekar', 'Birru Swamy', '3-125, Nallabelly, wardhannapeta, Warangal', 'Near court chowrasta, Karimnagar', 505001, 'Telangana', 'Karim Nagar', 'Karimnagar', '0000-00-00', 0, ' Telugu, English,', 0, 'Degree', 'rajashekarbirru531@gmail.', '7032991390', 8374848280, '7032991390', 850244815721, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-02-10 13:02:12', NULL, NULL, 2),
(22, 1, 'trupti', 'abc', 'palakollu', 'hyderabad', 500081, 'state 1', 'city 1', 'const 1', '1995-01-06', 1, 'english, telugu', 1, 'btech', 'tptrupti@gmail.com', '7013907291', 7013907231, '7013907291', 132575862356, 13257586, '2014-02-03', 7013907291, '2019-02-05', '2050-02-05', '2050-02-05', 51651496465614, '2019-02-03', '2050-02-03', '2020-02-10 13:20:08', NULL, NULL, 2),
(23, 1, 'trupti', 'abc', 'palakollu', 'hyderabad', 500081, 'state 1', 'city 1', 'const 1', '1995-01-06', 1, 'english, telugu', 1, 'btech', 'tptrupti@gmail.com', '7013907291', 7013907231, '7013907291', 132575862356, 13257586, '2014-02-03', 7013907291, '2019-02-05', '2050-02-05', '2050-02-05', 51651496465614, '2019-02-03', '2050-02-03', '2020-02-10 13:29:38', NULL, NULL, 2),
(24, 1, 'trupti', 'abc', 'palakollu', 'hyderabad', 500081, 'state 1', 'city 1', 'const 1', '1995-01-06', 1, 'english, telugu', 1, 'btech', 'tptrupti@gmail.com', '7013907291', 7013907231, '7013907291', 132575862356, 13257586, '2014-02-03', 7013907291, '2019-02-05', '2050-02-05', '2050-02-05', 51651496465614, '2019-02-03', '2050-02-03', '2020-02-10 13:36:16', NULL, NULL, 2),
(25, 1, 'trupti', 'pawar', 'palakollu', 'hyderabad', 500081, 'state 1', 'city 1', 'const 1', '1995-01-06', 1, 'english, telugu', 1, 'btech', 'tptrupti@gmail.com', '7013907291', 7013907231, '7013907291', 132575862356, 13257586, '2014-02-03', 7013907291, '2019-02-05', '2050-02-05', '2050-02-05', 51651496465614, '2019-02-03', '2050-02-03', '2020-02-10 13:45:44', NULL, NULL, 2),
(26, 1, 'trupti', 'pawar', 'iVBORw0KGgoAAAANSUhEUgAAAUwAAABVCAYAAAAiwN24AAAAAX', 'hyderabad', 500081, 'state 1', 'city 1', 'const 1', '1995-01-06', 1, 'english, telugu', 1, 'btech', 'tptrupti@gmail.com', '7013907291', 7013907231, '7013907291', 132575862356, 13257586, '2014-02-03', 7013907291, '2019-02-05', '2050-02-05', '2050-02-05', 51651496465614, '2019-02-03', '2050-02-03', '2020-02-10 13:50:17', NULL, NULL, 2),
(27, 1, 'trupti', 'pawar', 'iVBORw0KGgoAAAANSUhEUgAAAUwAAABVCAYAAAAiwN24AAAAAX', 'hyderabad', 500081, 'state 1', 'city 1', 'const 1', '1995-01-06', 1, 'english, telugu', 1, 'btech', 'tptrupti@gmail.com', '7013907291', 7013907231, '7013907291', 132575862356, 13257586, '2014-02-03', 7013907291, '2019-02-05', '2050-02-05', '2050-02-05', 51651496465614, '2019-02-03', '2050-02-03', '2020-02-10 14:00:49', NULL, NULL, 2),
(28, 1, 'trupti', 'pawar', 'palakollu', 'hyderabad', 500081, 'state 1', 'city 1', 'const 1', '1995-01-06', 1, 'english, telugu', 1, 'btech', 'tptrupti@gmail.com', '7013907291', 7013907231, '7013907291', 132575862356, 13257586, '2014-02-03', 7013907291, '2019-02-05', '2050-02-05', '2050-02-05', 51651496465614, '2019-02-03', '2050-02-03', '2020-02-10 14:05:56', NULL, NULL, 2),
(29, 1, 'trupti', 'pawar', 'palakollu', 'hyderabad', 500081, 'state 1', 'city 1', 'const 1', '1995-01-06', 1, 'english, telugu', 1, 'btech', 'tptrupti@gmail.com', '7013907291', 7013907231, '7013907291', 132575862356, 13257586, '2014-02-03', 7013907291, '2019-02-05', '2050-02-05', '2050-02-05', 51651496465614, '2019-02-03', '2050-02-03', '2020-02-10 14:14:48', NULL, NULL, 2),
(30, 1, 'trupti', 'pawar', 'palakollu', 'hyderabad', 500081, 'state 1', 'city 1', 'const 1', '1995-01-06', 1, 'english, telugu', 1, 'btech', 'tptrupti@gmail.com', '7013907291', 7013907231, '7013907291', 132575862356, 13257586, '2014-02-03', 7013907291, '2019-02-05', '2050-02-05', '2050-02-05', 51651496465614, '2019-02-03', '2050-02-03', '2020-02-10 14:24:40', NULL, NULL, 2),
(31, 0, 'New vehicle', 'hshsbbshs', 'bdhdhhd', 'zhsjjndhd', 500081, 'Telangana', 'Hyderabad', 'Asifabad', '1997-02-10', 0, ' Telugu, Hindi, English,', 0, 'zhshshhd', 'dafag@fagsh.njb', '4646376697', 7664646949, '4646466499', 464646496464, 0, '0000-00-00', 0, '2002-02-10', '2002-02-10', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-02-10 14:44:06', NULL, NULL, 2),
(32, 1, 'Thirupathi Naidu', 'Peddaiah', 'kammavaripalli', 'subhash nagar', 516501, 'Andhra Pradesh', 'Cuddapah', 'Tirupati', '0000-00-00', 0, ' Telugu,Telugu', 0, 'inter', 'thirupathinaidu0526@gmail', '9182754045', 9963906961, '9182754045', 630053691186, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-02-17 10:28:46', NULL, NULL, 2),
(33, 1, 'Thota Sreenivasulu', 'Thota Nagaraju', '3/73/C', '3/73/C', 517501, 'Andhra Pradesh', 'Chittoor', 'Tirupati', '0000-00-00', 0, ' Telugu, English,Telugu,English', 1, 'ssc', 'thotasrinivasulu57@gmail.', '7036171143', 8885638880, '7036171143', 252825575485, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 16073489, '0000-00-00', '0000-00-00', '2020-02-18 11:07:04', NULL, NULL, 2),
(34, 1, 'GODDUGORLA SAI KUMAR', 'G KOTESH', '2-224', 'Konijerla, khammam', 507305, 'Telangana', 'Khammam', 'Khammam', '0000-00-00', 0, ' Telugu, English,', 1, 'ITI', 'saieshwar974@gmail.com', '6281191403', 8374151967, '8374151967', 452184124305, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '2003-12-18', '0000-00-00', '2020-02-19 06:45:33', NULL, NULL, 2),
(35, 1, 'MANIKANTA', 'APPALARAJU', '4-98/1 KAJULURU MANDALAM MANJERA', '4-98/1 KAJULURU MANDALAM MANJERA', 533468, 'Andhra Pradesh', 'East Godavari', 'Kakinada City', '0000-00-00', 0, ' Telugu, English,', 1, 'SSC', 'mani.081253@gmail.com', '9550001781', 8886160682, '9550001781', 306856890003, 0, '0000-00-00', 0, '2019-06-18', '0000-00-00', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-02-25 05:52:16', NULL, NULL, 2),
(36, 1, 'SRINIVAS', 'Srinivas', '8-4-372/373/4, sai baba Nagar', '8-4-372/373/4, sai baba Nagar', 500018, 'Telangana', 'K.V.Rangareddy', 'Khairatabad', '0000-00-00', 1, ' Telugu, Hindi, English,', 0, 'Mba', 'nextclickhr008@gmail.com', '8919195804', 8919195804, '8919195804', 344145627891, 0, '0000-00-00', 0, '2014-11-13', '2020-11-13', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-02-26 07:03:55', NULL, NULL, 2),
(37, 1, 'sunija', 'prasad', '2-143', '2-143', 518502, 'Andhra Pradesh', 'Kurnool', 'Pithapuram', '0000-00-00', 0, ' Telugu, English,', 0, 'ssc', 'tsilvia37@gmail.com', '9390755642', 9390755642, '9390755642', 514850574592, 124456, '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-02-11', 123456, '2019-07-10', '2020-11-26', '2020-02-28 06:05:00', NULL, NULL, 2),
(38, 1, 'sai Krishna', 'shanker', '10-133, Hyderabad', '10-133,skzr', 504293, 'Telangana', 'Adilabad', 'Khanapur', '2020-02-28', 0, ' Hindi, English,', 0, 'b.tech', 'saisubba@gmail.com', '8977422783', 8977422783, '8977422783', 123456987, 0, '0000-00-00', 123456987, '2020-02-28', '2020-02-28', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-02-28 11:42:30', NULL, NULL, 2),
(39, 1, 'Manikanta', 'Rajkumar', 'mfdhhhgbncvbbbbcvvb', 'ngfgjhjdhhhghfvg', 500072, 'Telangana', 'Hyderabad', 'Adilabad', '1991-02-27', 0, ' Telugu, Hindi, English,', 1, 'mtech', 'manikanta.grepthor@gmail.', '9636555555', 9631445585, '9634748888', 934256895214, 0, '2020-02-29', 963457588, '1992-02-28', '2023-02-28', '2017-02-28', 852412375, '2017-02-28', '2020-02-29', '2020-02-28 12:28:48', NULL, NULL, 2),
(40, 1, 'k Prashanth', 'rajeshwar', '123 Quthbullapur, hyd', '123 Quthbullapur, hyd', 500055, 'Telangana', 'Hyderabad', 'Quthballapur', '0000-00-00', 0, ' Telugu, Hindi, English,', 0, 'degree', 'kittuprashanth96@gmail.co', '8309979216', 8309979216, '8309979216', 589625842584, 1234825438, '0000-00-00', 0, '2020-01-20', '2038-01-20', '0000-00-00', 133527256, '0000-00-00', '0000-00-00', '2020-02-28 12:58:24', NULL, NULL, 2),
(41, 1, 'Akkkapolu shyam sundar rao', 'satyam', '11-57 velaga valasa village alajangi post bobbili ', '11-57 velaga valasa village alajangi post bobbili ', 535558, 'Andhra Pradesh', 'Vizianagaram', 'Visakhapatnam East', '0000-00-00', 0, ' Telugu,', 1, 'degreebcom', 'shyamakkapolu@gmail.com', '9182139750', 9182139750, '9182139750', 484079458304, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '2009-05-24', 3397, '2019-05-09', '0000-00-00', '2020-02-28 13:22:03', NULL, NULL, 2),
(42, 0, 'SARAPUHBHANUPRASADDORA', 'SARAPU NAGANNA DORA', '2-15,YERRAMPALEM, GANGAVARAM MANDAL, EAST GODAVARI', '3-110,ST COLONY, ADDATEEGALA, EAST GODAVARI DISTRI', 533428, 'Andhra Pradesh', 'East Godavari', 'Rampachodavaram (ST)', '0000-00-00', 0, ' Telugu, English,', 0, 'DIED', 'shbhanuprasaddora@gmail.c', '9163029361', 6302936148, '9491543656', 502075337356, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-02-29 07:20:10', NULL, NULL, 2),
(43, 0, 'SARAPUHBHANUPRASADDORA', 'SARAPU NAGANNA DORA', '2-15,YERRAMPALEM, GANGAVARAM, EAST GODAVARI', '3-110, ST COLONY, ADDATEEGALA, EAST GODAVARI', 533428, 'Andhra Pradesh', 'East Godavari', 'Rampachodavaram (ST)', '0000-00-00', 0, ' Telugu, English,', 0, 'DIED', 'shbhanuprasaddora@gmail.c', '9491543656', 6302936148, '9491543656', 502075337356, 0, '0000-00-00', 0, '2018-09-17', '2038-09-16', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-02-29 07:33:59', NULL, NULL, 2),
(44, 1, 'SARAPUHBHANUPRASADDORA', 'SARAPU NAGANNA DORA', '2-15, YERRAMPALEM, GANGAVARAM, EAST GODAVARI', '3-110, ST COLONY, ADDATEEGALA VILLAGE, EAST GODAVA', 533428, 'Andhra Pradesh', 'East Godavari', 'Rampachodavaram (ST)', '0000-00-00', 0, ' Telugu, English,', 0, 'DIED', 'shbhanuprasaddora@gmail.c', '9491543656', 6302936148, '9491543656', 502075337356, 0, '2019-08-15', 0, '2018-09-17', '2038-09-19', '2020-03-04', 0, '2019-08-10', '2020-08-09', '2020-02-29 08:37:52', NULL, NULL, 2),
(45, 1, 'Akkapolu shyam sundar rao', 'akkapolu satyam', '11-57 velaga valasa village alajangi post bobbili ', '1-57 velaga valasa village alajangi post bobbili m', 535558, 'Andhra Pradesh', 'Vizianagaram', 'Visakhapatnam East', '0000-00-00', 0, ' Telugu,', 1, 'degreeb.comcomputers', 'shyamakkapolu@gmail.com', '9182139750', 9182139750, '9182139750', 484079458304, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '2019-05-09', '2024-05-08', '2020-03-02 07:28:51', NULL, NULL, 2),
(46, 1, 'vema Naga ragava venkatesh', 'Mohan rao', 'r agraharam', '24/4/35', 522003, 'Andhra Pradesh', 'Guntur', 'Guntur East', '0000-00-00', 0, ' Telugu, Hindi, English,', 0, 'tenth', 'vema.venki.1161@gmail.com', '8142881161', 8340070790, '8142881161', 365182056399, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-06-30', 0, '0000-00-00', '2025-12-12', '2020-03-02 08:10:34', NULL, NULL, 2),
(47, 1, 'pampana satyanarayana', 'pampana trimurthulu', '2-6,ramalayam street, peravali mandalam, peravali', '2-6,ramalayam street, peravali mandalam, peravali', 534328, 'Andhra Pradesh', 'West Godavari', 'Tanuku', '0000-00-00', 0, ' Telugu, English,', 0, 'ssc', '7342satya@gmail.com', '7569319141', 9908689376, '7569319141', 635014749645, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 5813890, '0000-00-00', '0000-00-00', '2020-03-02 10:16:32', NULL, '2020-03-20 09:58:36', 2),
(48, 1, 'GAJJALA VIJAYA BHASKAR REDDY', 'G RAMA SUBBA REDDY', '1/111, KAMMAVARI PALLI, CHINTAKOMMA DINNE, KADAPA', '1/111, KAMMAVARI PALLI, CHINTAKOMMA DINNE, KADAPA', 516003, 'Andhra Pradesh', 'Cuddapah', 'Kamalapuram', '0000-00-00', 0, ' Telugu,', 0, 'ITI', 'polathalav@gmail.com', '9848482231', 6281422551, '6281422551', 278130225597, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 105379362, '0000-00-00', '0000-00-00', '2020-03-02 11:08:33', NULL, NULL, 2),
(49, 1, 'MUDAVATH KRISHNA NAIK', 'M RAJU NAIK', 'SEETHAMMA THANDA VILLLAGE PEAPULLY MANDAL BURUGULA', 'SOMISETTI COMPLEX OPPOSITE RAJ THEATER', 518002, 'Andhra Pradesh', 'Kurnool', 'Kurnool', '0000-00-00', 0, ' Telugu, Hindi, English,Banjara,ODIA', 0, 'degree', 'sandhyakrishna2296@gmail.', '7981306180', 9000200398, '9000200398', 528700482730, 0, '2019-01-01', 0, '2019-08-31', '2030-08-30', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-03-02 12:05:45', NULL, NULL, 2),
(50, 1, 'VENKATESH GANDHAVADI', 'NAGESWARA RAO G', '7/1a, thogurupeta, RAJAMPET', 'Mr palli, srikrishna nagar, TIRUPATI', 517501, 'Andhra Pradesh', 'Chittoor', 'Tirupati', '1982-12-06', 0, ' Telugu, Hindi, English,Tamil', 1, 'b.com', 'gv.061282@gmail.com', '9573116105', 9573116105, '9573116105', 744806429683, 0, '2019-11-14', 0, '2019-11-05', '2027-10-21', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-03-02 12:09:19', NULL, NULL, 2),
(51, 1, 'Srikanth Dharavath', 'kanaka Dharavath', 'H NO:3-18 Mangalithanda (village),Chimirayala(Post', 'H NO:3-18 Mangalithanda (village),Chimirayala(Post', 508206, 'Telangana', 'Nalgonda', 'Kodad', '0000-00-00', 0, ' Telugu, English,', 0, 'Degree', 'srikanthdharavath817@gmai', '8179091231', 9014615316, '8179091231', 652027556640, 0, '2019-02-09', 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-03-03 04:18:23', NULL, NULL, 2),
(52, 1, 'jagadish kumar', 'ramankaneyulu', 'devinagar', '1st line', 520003, 'Andhra Pradesh', 'Krishna', 'Vijayawada Central', '0000-00-00', 0, ' Telugu, Hindi, English,', 1, 'M.com', 'jagadeeshkumarjonnalagadd', '6303221199', 8886968362, '9490908534', 870968110264, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-03-04 01:57:56', NULL, NULL, 2),
(53, 1, 'PIDATHALA VAMSEE CHANDRA', 'P CH MASTHANAIAH', '14-57 wagon work shop colony guntupalli Krishna di', 'same as above', 521241, 'Andhra Pradesh', 'Krishna', 'Mylavaram', '1987-08-14', 0, ' Telugu, Hindi, English,', 1, 'B.Com', 'vamsee14@gmail.com', '9290440754', 9290440754, '9290440754', 948876322880, 0, '2019-12-27', 0, '2009-11-07', '2029-11-06', '2020-06-26', 122344445, '2019-12-27', '2025-12-26', '2020-03-04 08:33:12', NULL, NULL, 2),
(54, 1, 'Balam sivanagu', 'balance subbarao', '4-7-2/2,ward no 39,durgapuram,sivarao Peta, bhimav', '2-5,29 ward, gollathippani road, bhimavaram, west ', 534202, 'Andhra Pradesh', 'West Godavari', 'Bhimavaram', '0000-00-00', 0, ' Telugu, English,', 0, 'ssc', 'sivanaga9542241288@gmail.', '6301514113', 6301514113, '6301514113', 468452764041, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 191144453, '0000-00-00', '0000-00-00', '2020-03-04 12:46:10', NULL, NULL, 2),
(55, 1, 'KOTLA THAMMI', 'karllappa', '2-106 Konijerla', 'Konijerla  mandal', 507305, 'Telangana', 'Khammam', 'Khammam', '0000-00-00', 0, ' Telugu, English,', 0, 'Inter', '8367605037s@gmail.com', '8367605037', 6303483951, '8367605037', 849642443176, 0, '0000-00-00', 0, '2020-02-25', '0000-00-00', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-03-05 11:06:14', NULL, NULL, 2),
(56, 1, 'VEMULA  ASHOK', 'VERABHADRAM', '2-246', 'Khammam urben', 507002, 'Telangana', 'Khammam', 'Khammam', '0000-00-00', 0, ' Telugu, English,', 0, 'Degree', 'Sathish.vemula1823@gmail.', '9515960461', 9515960461, '9515960461', 863544597193, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-03-05 11:28:24', NULL, NULL, 2),
(57, 1, 'Manmohan Reddy', 'Rama krishna Reddy', 'subramanya street yanam', 'Indrapalem kakinada', 533006, 'Andhra Pradesh', 'East Godavari', 'Kakinada City', '0000-00-00', 0, ' Telugu, English,', 0, 'inter', 'man420210@gmail.com', '8008607475', 8333014430, '8008607475', 665119407874, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 103481881, '0000-00-00', '0000-00-00', '2020-03-05 15:55:02', NULL, NULL, 2),
(58, 1, 'muthineni venkatanarasimharao', 'muthineni mohanrao', 'H NO:4-46, KAKARLA VILLAGE JULURUPADU MANDAL', 'H NO:4-46, KAKARLA VILLAGE JULURUPADU MANDAL', 507166, 'Telangana', 'Khammam', 'Kothagudem', '0000-00-00', 0, ' Telugu,', 1, 'INTER', 'muthinenivenkat81@gmail.c', '9652190503', 8247768500, '9652190503', 232738360184, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 114569260, '0000-00-00', '0000-00-00', '2020-03-06 11:03:42', NULL, NULL, 2),
(59, 1, 'Nenavath Hari Kishan', 'Nenavath Kishan', '2-4-80/2', 'venkatapur tanda vikarabad', 501101, 'Telangana', 'Hyderabad', 'Vikarabad', '1998-05-19', 0, ' Telugu, Hindi,no', 0, 'no', 'nenavathhari1998@gmail.co', '9676427641', 9182268578, '9676427641', 756954114940, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-03-07 06:03:58', NULL, NULL, 2),
(60, 1, 'BURJUKINDI KARUNAKAR REDDY', 'BURJUKINDI NARSIMHA REDDY', '1-28 dilalpur gajwel siddipet telangana', '1-28 dilalpur gajwel siddipet telangana', 502278, 'Telangana', 'Medak', 'Gajwal', '0000-00-00', 0, ' Telugu, Hindi, English,', 0, 'inter', 'karnakarreddy2208@gmail.c', '8978722208', 8790768627, '8978722208', 266577644406, 0, '2018-09-15', 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '2018-08-24', '2019-09-23', '2020-03-07 09:28:49', NULL, NULL, 2),
(61, 1, 'rapuri narendra', 'rapuri musalaiah', 'No. 09, ST colony, udayagidi mandalm, tirumalpuram', 'No. 09, ST colony, udyagiri mandalam. tirumalapura', 524236, 'Andhra Pradesh', 'Nellore', 'Udayagiri', '1987-07-15', 0, ' Telugu, English,', 1, 'SSC', 'rapurinarendra1987@gmail.', '9676845350', 9676845350, '9676845350', 540824295633, 0, '2015-04-10', 0, '2016-07-13', '2023-04-10', '2020-12-06', 151500332, '2019-07-31', '2020-07-30', '2020-03-10 05:22:55', NULL, NULL, 2),
(62, 1, 'ponguru srinivasulu', 'ponguru subbarayudu', 'vengamamba puram, kayuru', 'vengamambapuram, kayuru', 524404, 'Andhra Pradesh', 'Nellore', 'Nellore Rural', '1987-06-06', 0, ' Telugu, English,', 1, 'SSC', 'pongurusrinivasulu1987@gm', '8466961874', 8466961874, '8466961874', 8466961874, 0, '2012-10-20', 0, '2013-04-16', '2033-04-15', '2020-11-20', 180048083, '2019-11-11', '2020-11-10', '2020-03-10 05:41:04', NULL, NULL, 2),
(63, 1, 'manubrolu ramprasad', 'manubrolu mastanrao', '28-4-4/2, gajulavari vedi, guntur', '28-4-4/2, gajulavari vedi,  guntur', 522004, 'Andhra Pradesh', 'Guntur', 'Guntur East', '1987-06-18', 0, ' Telugu, English,', 1, 'SSC', 'manubroluramprasad1987@gm', '9573410411', 9573410411, '9573410411', 593147654583, 0, '2019-10-03', 0, '2014-12-22', '2034-08-29', '2020-11-25', 5190634, '2019-10-03', '2024-10-02', '2020-03-10 08:28:50', NULL, NULL, 2),
(64, 1, 'MUDAVATH GANGADHAR NAIK', 'MUDAVATH RUPLA NAIK', '2 53 GULAMALIbad THANDA, PASUPULA POST, BANAGANAPA', 'housing board,doctors colony, \nkurnool', 518220, 'Andhra Pradesh', 'Kurnool', 'Kurnool', '1993-05-04', 0, ' Telugu, English,', 0, 'DEGREE', 'ganga.gangadhar9sbi@gmail', '8886343132', 9441009787, '8886343132', 659474651807, 0, '0000-00-00', 0, '2014-04-16', '2034-04-20', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-03-10 11:59:11', NULL, NULL, 2),
(65, 1, 'manubrolu ram prasad', 'manubrolu masthan rao', '28-4-4/2, gajulavari vedi, Guntur', '27-4-4/2, gajulavari vedi, Guntur', 522004, 'Andhra Pradesh', 'Guntur', 'Guntur East', '1987-06-18', 0, ' Telugu, English,', 1, 'SSC', 'manubroluramprasad1987@gm', '9573410411', 9573410411, '9573410411', 593147654583, 0, '0000-00-00', 0, '2014-12-22', '2034-08-29', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-03-10 12:05:18', NULL, NULL, 2),
(66, 1, 'ummaka ramakrishna', 'ummaka ravisankar', 'hanumanthagiri, Gudemcheruvu, jamlamadugu', 'hanumanthagiri, gudemcheruvu, jamlamadugu', 516434, 'Andhra Pradesh', 'Cuddapah', 'Jammalamadugu', '1996-06-25', 0, ' Telugu, English,', 0, 'SSC', 'ummakramakrishna196@gmail', '8074298756', 8074298756, '8074298756', 202250524108, 0, '0000-00-00', 0, '2020-02-26', '2036-06-24', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-03-10 12:18:32', NULL, NULL, 2),
(67, 1, 'PEESA GOPALA RAO', 'VENKATESWARAO', '23-11-116, NRP ROAD SATYANARAYANAPURAM VIJAYAWADA', '23-11-116, NRP ROAD SATYANARAYANAPURAM VIJAYAWADA', 520011, 'Andhra Pradesh', 'Krishna', 'Vijayawada Central', '0000-00-00', 0, ' Telugu, Hindi, English,no', 0, 'ssc', 'peesagopi407@gmail.com', '7416955658', 9603622314, '7416955658', 590178470781, 0, '0000-00-00', 0, '2019-08-08', '2038-12-31', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-03-10 18:09:31', NULL, NULL, 2),
(68, 1, 'PEESA GOPALA RAO', 'VENKATESWARAO', '23-11-116, NRP ROAD SATYANARAYANAPURAM VIJAYAWADA', '23-11-116, NRP ROAD SATYANARAYANAPURAM Vijayawada', 520011, 'Andhra Pradesh', 'Krishna', 'Vijayawada Central', '0000-00-00', 0, ' Telugu,no', 0, 'ssc', 'peesagopi407@gmail.com', '7416955658', 9603622314, '7416955658', 590178470781, 0, '0000-00-00', 0, '0000-00-00', '2038-12-31', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-03-11 07:03:04', NULL, NULL, 2),
(69, 1, 'Peesa Gopal  Rao', 'Venkateswarao Rao', 'D.No 23-11-116, NRP Road , Satyanarayanapuram. Vij', 'D.No 23-11-116, NRP Road , Satyanarayanapuram , Vi', 520011, 'Andhra Pradesh', 'Krishna', 'Vijayawada Central', '0000-00-00', 0, ' Telugu, English,', 0, 'S,S,C', 'peesagopi407@gmail.com', '7416955658', 7416955658, '7416955658', 590178470789, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 417055, '0000-00-00', '0000-00-00', '2020-03-11 13:55:56', NULL, NULL, 2),
(70, 1, 'm r n navin', 'm n swamy', '7-98,dommeru,wgdt,kovvurmandal', '29-1-23/c,devichowck, rajahmundry', 533103, 'Andhra Pradesh', 'East Godavari', 'Rajahmundry City', '0000-00-00', 0, ' Telugu, English,', 1, 'mba', 'mrn.naveen007@gmail.com', '9032122345', 8977887789, '9032122345', 951183149635, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-08-03', 167, '0000-00-00', '0000-00-00', '2020-03-11 15:32:39', NULL, NULL, 2),
(71, 1, 'KANUMALLA JAIRAM', 'NARAYANA', '41-8-54, Veerabhadra nagar, ALLUMINIUM workers col', '41-8-54, Veerabhadra nagar, ALLUMINIUM workers col', 533101, 'Andhra Pradesh', 'East Godavari', 'Rajahmundry City', '0000-00-00', 0, ' Telugu,', 1, 'ssc', 'ravirajankanakala@gmail.c', '9618636357', 9848552799, '9618636357', 856599384524, 0, '2019-08-09', 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-03-12 06:45:30', NULL, '2020-03-14 08:12:42', 2),
(72, 1, 'balija kumar', 'b parameswarappa', '86-324-8,Partha Sarathi nagar,kurnool', 'Partha Sarathi nagar, kurnool', 518002, 'Andhra Pradesh', 'Kurnool', 'Kurnool', '0000-00-00', 0, ' Telugu, Hindi, English,kannada', 1, 'degree', 'balijakumar@gmail.com', '9703465119', 9703465119, '9703465119', 68101656524, 0, '0000-00-00', 0, '2005-06-25', '0000-00-00', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-03-12 12:49:24', NULL, NULL, 2),
(73, 1, 'GUDIPADU venkata gopinath rao', 'Venkata rao', '46-801F,A camp,budhavarapeta,kurnool', '46-801F,A camp,budhavarapeta,kurnool', 518002, 'Andhra Pradesh', 'Kurnool', 'Kurnool', '0000-00-00', 0, ' Telugu, Hindi,', 1, 'degree', 'gvgopinathrao@gmail.com', '9502609732', 7995482583, '9502609732', 846172930071, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-03-12 13:07:16', NULL, NULL, 2),
(74, 1, 'Meripo vinaybkumar', 'meripo devadaanam', '2-32-63,new colony, old town, tanuku, west godavar', '2-32-63.new colony, old town, tanuku, Andrea prade', 534211, 'Andhra Pradesh', 'West Godavari', 'Tanuku', '0000-00-00', 0, ' Telugu, English,', 1, 'ssc', 'devakiinternet@gmail.com', '8500191998', 8500191998, '8500191998', 873583352446, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 5194, '0000-00-00', '0000-00-00', '2020-03-13 06:14:37', NULL, NULL, 2),
(75, 1, 'Gudivaada Seshu', 'Gudivaada murali', 'kaalavagattu vuyyuru Vijayawada', 'kaalavagattu vuyyuru Vijayawada', 521165, 'Andhra Pradesh', 'Krishna', 'Penamaluru', '0000-00-00', 0, ' Telugu, Hindi, English,', 1, 'ssc', 'gudivadaseshu@gmail.com', '9398741872', 8886394924, '8886394924', 870739066839, 0, '0000-00-00', 51620190033455, '2019-09-17', '2029-09-16', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-03-13 10:39:49', NULL, NULL, 2),
(76, 1, 'Deva srinu', 'Deva bhaskarao late', 'peda veedhi bhogapuram village \n bhoghapurammandal', 'pedaveedhi bhogapuram village bhogapuram mandal \nv', 535216, 'Andhra Pradesh', 'Vizianagaram', 'Nellimarla', '0000-00-00', 0, ' Telugu,', 0, 'ssc.inter.degreee', 'devasrinu1@gmail.com', '9182406184', 9966671608, '9966671608', 778654865282, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 417009, '0000-00-00', '0000-00-00', '2020-03-13 16:45:53', NULL, NULL, 2),
(77, 1, 'Andhe Gopi Ramana', 'And he Nageswara rao', '7-1-7,rajaka street, 30 ward, palakollu, w g Dist,', '7-1-7,rajaka street, 30 ward, palakollu, w g Dist,', 534260, 'Andhra Pradesh', 'West Godavari', 'Palakollu', '0000-00-00', 0, ' Telugu, English,', 1, 'ssc', 'gopia677@gmail.com', '9603874677', 9705838508, '9603874677', 681921308914, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 6905, '0000-00-00', '0000-00-00', '2020-03-14 05:45:24', NULL, NULL, 2),
(78, 1, 'sowjan', 'narsaiah', '1-1-116,kannalabasthi, bellampalli', '1-1-116,kannalabasthi, bellampalli', 504251, 'Telangana', 'Adilabad', 'Bellampalli', '0000-00-00', 0, ' Telugu, English,', 0, 'intermediate', 'sharath.nepal@gmail.com', '8125200410', 8686479012, '8125200410', 228685367011, 0, '0000-00-00', 0, '2019-12-30', '2020-06-29', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-03-14 05:50:18', NULL, NULL, 2),
(79, 1, 'kinda venkanna', 'bodaiah', 'maddirala(village,post),nuthankal (mandal) suryape', 'maddirala (village,post),nuthankal(mandal)suryapet', 508280, 'Telangana', 'Nalgonda', 'Suryapet', '0000-00-00', 0, ' Telugu, English,', 1, 'ssc', 'kondavenkat2@gmail.com', '9848128318', 9848128318, '9848128318', 572034149844, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-03-14 06:48:58', NULL, NULL, 2),
(80, 1, 'malavath anil', 'shankar', '10-23/1 , jakranpally thanda, jakranpalle, nizamab', '10-23/1 jakranpally thanda, jakranpalle, nizamabad', 503175, 'Telangana', 'Nizamabad', 'Armoor', '0000-00-00', 0, ' Telugu,', 0, 'intermediate', 'malavathanil66@gmail.com', '7337208054', 7337208054, '7337208054', 882343233927, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-03-14 12:34:12', NULL, NULL, 2),
(81, 1, 'Sravanapelli Santhosh', 'Sravanapelli Rajesham', '#8-2-425, Kattarampur,Near Kakatiya Techno School,', '#8-2-425, Kattarampur,Near Kakatiya Techno School,', 505001, 'Telangana', 'Karim Nagar', 'Karimnagar', '0000-00-00', 0, ' Telugu, Hindi, English,', 0, 'B.Sc', 'sravanapellisanthosh3453@', '9059465914', 9542867839, '9059465914', 555363912293, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 18244099, '0000-00-00', '0000-00-00', '2020-03-15 13:08:30', NULL, NULL, 2),
(82, 1, 'dosapati  sureshkumar', 'dosapati latchaiah', 'H.no:1- 11:nadigudem(post ,mandal ,village)nalgond', 'H.no:1- 11,nadigudem (post,mandal,village)nalgonda', 508234, 'Telangana', 'Nalgonda', 'Kodad', '0000-00-00', 0, ' Telugu,', 1, 'ssc', 'dosapatisureshkumar78@gma', '7989009163', 7989009163, '7989009163', 432743647406, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-03-16 01:40:15', NULL, NULL, 2),
(83, 1, 'Manikanta', 'Soma Kumar', 'mkbhfvfhbhfbbbjj', 'ncfhnfjjgjhjhvbbbbbb', 500072, 'Telangana', 'Hyderabad', 'Mancherial', '2009-03-19', 0, ' Telugu, Hindi, English,Tamil,marati', 1, 'Mtech', 'manikantaboddu282@gmail.c', '9636555858', 9635578685, '9635555666', 963214578955, 0, '1993-03-25', 0, '2003-03-21', '1987-03-19', '2022-03-10', 963886585, '2020-06-26', '2021-06-26', '2020-03-16 11:17:32', NULL, NULL, 2),
(84, 1, 'Gaddala Shekar', 'Gaddala Krishna', 'HNO 5-55 Ramakrishnapuram village, Chintakani mand', 'HNO 5-55 Ramakrishnapuram village, Chintakani mand', 507208, 'Telangana', 'Khammam', 'Madhira', '1990-07-03', 0, ' Telugu, English,', 0, 'Graduation', 'gaddalashekar@gmail.com', '9154842787', 8247567927, '9154842787', 406687699962, 0, '2017-08-04', 0, '2017-03-15', '2037-03-14', '2020-09-14', 218843, '2019-09-06', '2020-09-08', '2020-03-17 16:18:54', NULL, NULL, 2),
(85, 1, 'Bandaru Srinivasu', 'suryanarayana bandaru', 's/o bandaru suryanarayana,\n\n\n\n\n\n\n\n3-129/1,posamma ', 's/o bandaru suryanarayana,3-129/1,posamma gudi vee', 533307, 'Andhra Pradesh', 'East Godavari', 'Mandapeta', '0000-00-00', 0, ' Telugu,', 0, 'intermediate', 'bsrinuvas741@gmail.com', '8978579415', 9542184366, '8978579415', 502252787874, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-03-19 10:52:58', NULL, NULL, 2),
(86, 1, 'srinivasu bandaru', 'suryanarayana bandaru', 's/o bandaru suryanarayana,3-129/1,posamma gudi vee', 's/o bandaru suryanarayana,3-129/1,posamma gudi vee', 533307, 'Andhra Pradesh', 'East Godavari', 'Mandapeta', '0000-00-00', 0, ' Telugu,', 0, 'intermediate', 'bsrinuvas741@gmail.com', '8978579415', 9542184366, '8978579415', 502252787874, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-03-19 11:03:53', NULL, NULL, 2),
(87, 1, 'Nomula Sagar', 'Venkanna', 'H- no:1- 137sc.colony,suryapet (mandal),thallakham', 'H- no:1- 137,sc ,colony,suryapet (mandal)thallakha', 508213, 'Telangana', 'Nalgonda', 'Suryapet', '0000-00-00', 0, ' Telugu, English,', 0, 'ssc', 'nomulasagar165@gmail.com', '7036044955', 7036044955, '7036044955', 679364662150, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-03-20 01:49:51', NULL, NULL, 2),
(88, 1, 'varthya raghunayak', 'varthya babia', '2-4-110', 'venkatapur colony v', 501101, 'Telangana', 'Hyderabad', 'Vikarabad', '0000-00-00', 0, ' Telugu, Hindi, English,', 1, 'tenth', 'varthyaraghunayak@gmail.c', '7989994641', 9703233361, '7989994641', 954235486058, 25, '0000-00-00', 0, '2019-01-23', '0000-00-00', '0000-00-00', 0, '2019-11-30', '2020-11-29', '2020-03-21 09:01:44', NULL, NULL, 2),
(89, 0, 'KOMMU CHAITANYA', 'KOMMU RAVI', '7_13 chennyapalem(v)karalapadu(p)piduguralla(m) gu', '7_13 chennyapalem(v)karalapadu(p)piduguralla(m) gu', 522437, 'Andhra Pradesh', 'Guntur', 'Gurajala', '0000-00-00', 0, ' Telugu, English,', 1, 'BSC.B.ED', 'kommuchaitanya927@gmail.c', '9502254478', 7013113768, '9502254478', 726103563586, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, '0000-00-00', '0000-00-00', '2020-03-25 03:35:02', NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `key` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_user_id` bigint(15) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_user_id` bigint(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0-Deleted 1-Active 2-Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`, `created_user_id`, `created_at`, `updated_user_id`, `updated_at`, `deleted_at`, `status`) VALUES
(1, 'system_name', 'Next Click Raider', 0, '2019-04-30 15:41:07', 1, '2020-01-06 13:41:07', NULL, 1),
(2, 'system_title', 'your success begins Here', 0, '2019-04-30 15:41:07', 1, '2020-01-06 13:41:07', NULL, 1),
(3, 'address', 'Hyderabad', 0, '2019-04-30 15:41:07', 1, '2020-01-06 13:41:07', NULL, 1),
(4, 'mobile', '8121815507', 0, '2019-04-30 15:41:07', 1, '2020-01-06 13:41:07', NULL, 1),
(5, 'system_email', 'info@gmail.com', 0, '2019-04-30 15:41:07', 1, '2019-10-16 16:27:32', NULL, 1),
(6, 'email_password', '123123', 0, '2019-04-30 15:41:07', 1, '2019-10-16 16:27:32', NULL, 1),
(7, 'sms_username', '123', 0, '2019-04-30 15:41:07', 1, '2019-10-16 14:44:03', NULL, 1),
(8, 'sms_sender', 'TRAI', 0, '2019-04-30 15:41:07', 1, '2019-10-16 14:44:03', NULL, 1),
(9, 'sms_hash', 'HASH', 0, '2019-04-30 15:41:07', 1, '2019-10-16 14:44:03', NULL, 1),
(10, 'privacy', '<h1 style=\"text-align: center;\"><span style=\"color:#008000\"><u><span style=\"font-size:22px\"><span style=\"font-family:lucida sans unicode,lucida grande,sans-serif\"><var><strong><em>MyPulse</em></strong></var></span></span></u></span></h1>\r\n', 0, '2019-04-30 15:41:07', NULL, '0000-00-00 00:00:00', NULL, 1),
(11, 'terms', '<p>hi this is for testing</p>\r\n', 0, '2019-04-30 15:41:07', NULL, '0000-00-00 00:00:00', NULL, 1),
(12, 'facebook', 'https://www.facebook.com/', 0, '2019-07-22 14:05:08', 1, '2020-01-06 13:41:07', NULL, 1),
(13, 'twiter', 'https://www.twiter.com/', 0, '2019-07-22 14:05:08', 1, '2020-01-06 13:41:07', NULL, 1),
(14, 'youtube', 'https://www.youtube.com/', 0, '2019-07-22 14:05:08', 1, '2020-01-06 13:41:07', NULL, 1),
(15, 'skype', 'http://skype.com/', 0, '2019-10-16 09:49:33', 1, '2020-01-06 13:41:07', NULL, 1),
(16, 'pinterest', 'http://pinterest.com/', 0, '2019-10-16 09:50:50', 1, '2020-01-06 13:41:07', NULL, 1),
(17, 'smtp_port', '587', 0, '2019-10-16 11:06:00', 1, '2019-12-04 12:33:39', NULL, 1),
(18, 'smtp_host', 'mail.akshayacomputers.com', 0, '2019-10-16 11:06:00', 1, '2019-12-04 12:33:39', NULL, 1),
(19, 'smtp_username', 'info@akshayacomputers.com', 0, '2019-10-16 11:06:42', 1, '2019-12-04 12:33:39', NULL, 1),
(20, 'smtp_password', 'akshaya@123', 0, '2019-10-16 11:06:42', 1, '2019-12-04 12:33:39', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `unique_id` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `wallet` decimal(14,2) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `list_id` bigint(20) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `created_user_id` bigint(20) DEFAULT NULL,
  `updated_user_id` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0 = deleted, 1 = Active, 2 = Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `unique_id`, `password`, `salt`, `email`, `wallet`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `list_id`, `first_name`, `last_name`, `company`, `phone`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`, `deleted_at`, `status`) VALUES
(1, '192.168.1.28', 'administrator', 'NCA0101', '$2y$08$NLgGBmEz2go4EPJcfu76QeBJLWAkAxQ9X9xjkF6Slb7O5eU29XK7W', '', 'admin@admin.com', '1500.00', '', NULL, NULL, 'Gp.hc8LGQ8iQoG3p10z8JO', 1268889823, 1584698291, 1, 0, 'Admin', 'istrator', 'ADMIN', '9988556555', NULL, 1, '2019-09-20 16:59:05', '2019-12-10 13:28:11', NULL, 1),
(4, '::1', 'mehargrepthor@gmail.com', 'NCE0101', '$2y$08$WR7Q2mQBklvZvPtkc7PicO3yxb29BJuH2Tw46sS2OzV5FqSHhRYyy', NULL, 'mehargrepthor@gmail.com', '0.00', 'c93d34093dbf5fb286569a20543025191ef05785', NULL, NULL, NULL, 1569911622, 1570284067, 1, 0, 'mehar', 'trinadh', NULL, '8522808784', NULL, 4, '2019-10-01 12:03:42', '2019-10-05 14:50:44', NULL, 1),
(7, '::1', 'tptrupti011@gmail.com', 'NCE0102', '$2y$08$ajiXpEQbW8dk0PQ0kvbREOLtgNLZ7C7dtBTM4wopUCWsU.5Xjne/q', NULL, 'tptrupti011@gmail.com', '0.00', 'cbb04aa690725d6ac1b589caf2104cd00a44dacd', NULL, NULL, NULL, 1570784942, NULL, 0, 0, 'truptip', 'istratorddf', NULL, '6303423888', NULL, NULL, '2019-10-11 14:39:02', NULL, NULL, 1),
(8, '::1', 'admisxn@gmail.com', 'NCE0103', '$2y$08$7j7QucjqKHsAQdX5PzKq4u0tJ3USNzUYDIIsCQ83CD7hW53VIr5jm', NULL, 'admisxn@gmail.com', '0.00', 'a4ebd3e3fa96a41d68def5cddaefd37105793832', NULL, NULL, NULL, 1571033157, NULL, 0, 0, 'truptip', 'istratorddf', NULL, '----------', NULL, 1, '2019-10-14 11:35:57', '2019-10-14 08:17:25', '2019-10-14 08:17:25', 1),
(9, '::1', 'admiassn@gamil.com', 'tp120101', '$2y$08$WDBjtryZQ/u8nUBdfDMCy.gBc9zkZZA3Mjobw5HAjUd80ES8WrjEW', NULL, 'admiassn@gamil.com', '0.00', '27e656a7e818d746101c14bde4e2e7bf8ebd7c7a', NULL, NULL, NULL, 1572685693, NULL, 0, 0, 'trupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 14:38:13', NULL, NULL, 1),
(10, '::1', 'adfd@gmail.com', 'tp120105', '$2y$08$fjoAiWNTy5iCXf9PMBBpXeKjD68dZKMJWxtKte22YfvRD7UcnKANm', NULL, 'adfd@gmail.com', '0.00', 'a1e5e69954edc77c4804671d73e769c89e2c07aa', NULL, NULL, NULL, 1572685904, NULL, 0, 0, 'trupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 14:41:44', NULL, NULL, 1),
(11, '::1', 'adfdbbbb@gmail.com', 'tp120106', '$2y$08$cn4hbj3U2W6o8H/g/Dit2OVTZgCHNQjePBrNz8MU2kwYB8loKUUrm', NULL, 'adfdbbbb@gmail.com', '0.00', 'e3898f732e85137231f755721222badd651490ee', NULL, NULL, NULL, 1572686075, NULL, 0, 0, 'trupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 14:44:35', NULL, NULL, 1),
(12, '::1', 'adfdbmbbb@gmail.com', 'tp120108', '$2y$08$KYY5AMxyH.IQnsGtBX2FvOyg.h2d9HmJXywOg/BVBLKOcVf5fB8k2', NULL, 'adfdbmbbb@gmail.com', '0.00', '44bbd56dce789d08e420c724e0588d9904f30a32', NULL, NULL, NULL, 1572686470, NULL, 0, 0, 'trupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 14:51:10', NULL, NULL, 1),
(13, '::1', 'abbb@gmail.com', 'tp120109', '$2y$08$cMoQURojKeD.whelR5UEP.JX5a3R1MJHRtduhcQkQKWGQYPNiZlXW', NULL, 'abbb@gmail.com', '0.00', 'd6877d1c3aa7c0cec0e6e286c6f342df571b8c8c', NULL, NULL, NULL, 1572687105, NULL, 0, 0, 'trupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 15:01:45', NULL, NULL, 1),
(14, '::1', 'abbb@gmmmail.com', 'tp120110', '$2y$08$E0iohywOkecFGwdTNQf8c.AYAT1hjGVzKdQFgC6pKrcUSQ9552qMa', NULL, 'abbb@gmmmail.com', '0.00', 'd2b649ee63e6047717649ca7e5d9fe629e7e75ae', NULL, NULL, NULL, 1572687147, NULL, 0, 0, 'trupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 15:02:27', NULL, NULL, 1),
(15, '::1', 'abbb@gnmmmail.com', 'tp120111', '$2y$08$CShLJmDwr99j3LuGHmDHbOokO4vUYz3sy/JviBX0VMZSaJotn1Q0G', NULL, 'abbb@gnmmmail.com', '0.00', 'd5fa96ec4ec121e17cba34b9f69cb16929a10003', NULL, NULL, NULL, 1572687299, NULL, 0, 0, 'trupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 15:04:59', NULL, NULL, 1),
(16, '::1', 'abnbb@gnmmmail.com', 'tp120112', '$2y$08$njogmBx.Pgbu6R43YCqH2O2glTZpJTDNkbwXVotMxRIwASpdQXDt2', NULL, 'abnbb@gnmmmail.com', '0.00', '1a2320a0454d7a3cf2ee5d78f6887fd531a99a66', NULL, NULL, NULL, 1572687364, NULL, 0, 0, 'trupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 15:06:04', NULL, NULL, 1),
(17, '::1', 'abnbb@gnmmmmail.com', 'tp120113', '$2y$08$t1yzCPehu6SgkcCdfxJ10uFcBwqSJAZ5xGWN5vr2Nxu0SdcV2bQby', NULL, 'abnbb@gnmmmmail.com', '0.00', '8cd8b31b6a68efd3dc0b6bc3ca1b07b8aa3f813e', NULL, NULL, NULL, 1572687493, NULL, 0, 0, 'trupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 15:08:13', NULL, NULL, 1),
(18, '::1', 'abnk@gnbmmmmail.com', 'tp120114', '$2y$08$tBvzxJrgHESHJzc7DozDV.sPsgbgdoBmpz42kU/06CTwx6w3eEwUu', NULL, 'abnk@gnbmmmmail.com', '0.00', 'e58d6c72269307189265177037b18aad9036f928', NULL, NULL, NULL, 1572687548, NULL, 0, 0, 'trupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 15:09:08', NULL, NULL, 1),
(19, '::1', 'abnkbnb@gnbmmmmail.com', 'tp120115', '$2y$08$pg1/WldcUXD3cbaWOfkDeexYQozEZ56KlN7.avM8sN9twXwHEjdcG', NULL, 'abnkbnb@gnbmmmmail.com', '0.00', '906ec5ce772423cf5c2eb03190caa3c7eb55791b', NULL, NULL, NULL, 1572687946, NULL, 0, 0, 'bnupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 15:15:46', NULL, NULL, 1),
(20, '::1', 'trupti5600@gmail.com', 'tp120117', '$2y$08$u7TP3yQp8/y7Qzqi9KCeyujE9iZzVLWVRbre86.0q2iR6wPzfN/8q', NULL, 'trupti5600@gmail.com', '0.00', 'e930e565a3762ca552678de333b14fe28d8c1dbc', NULL, NULL, NULL, 1572688154, NULL, 0, 0, 'bnupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 15:19:14', NULL, NULL, 1),
(21, '::1', 'trupti5601@gmail.com', 'tp120120', '$2y$08$6UeniKROvCmPaCNIBdO4Le8p3Hpay8TeuKv.pOiIiUC9VNKAy486m', NULL, 'trupti5601@gmail.com', '0.00', 'fdb3fae079a5a97d1a0aacd965667cd88762b158', NULL, NULL, NULL, 1572688602, NULL, 0, 0, 'bnupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 15:26:42', NULL, NULL, 1),
(22, '::1', 'trupti5602@gmail.com', 'tp120121', '$2y$08$ituXoTYWbnkR6StAv1O/..YGsxsnbQjboUGn9LtO.ZThiTDL5uRd6', NULL, 'trupti5602@gmail.com', '0.00', 'ff1596a8833968d832e84ddedaccbdd8e730f525', NULL, NULL, NULL, 1572688850, NULL, 0, 0, 'bnupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 15:30:50', NULL, NULL, 1),
(23, '::1', 'trupti5603@gmail.com', 'tp120122', '$2y$08$huAKCS3bgPKCDmt/jSVYy.nAuvZccyr/muFBBZhOKKcO5gc0A0lJi', NULL, 'trupti5603@gmail.com', '0.00', '77157d62d20358799e1acb87069260c20c7e00a6', NULL, NULL, NULL, 1572688917, NULL, 0, 0, 'bnupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 15:31:57', NULL, NULL, 1),
(24, '::1', 'trupti5v603@gmail.com', 'tp120123', '$2y$08$J.fGcIvPsyVA0BH.YC8Sved3xGiUJcxuvzj7/KT93.q8Hr6beQi6W', NULL, 'trupti5v603@gmail.com', '0.00', '3b5281807f1fe0530ddbdc6dc0970fe618196808', NULL, NULL, NULL, 1572689013, NULL, 0, 0, 'bnupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 15:33:33', NULL, NULL, 1),
(25, '::1', 'trupti58603@gmail.com', 'tp120124', '$2y$08$M4G79wjrdOdgtTeKVwf5jOxUY6F9T.WxvwQ03NOf5JqY/e5ZKNj6C', NULL, 'trupti58603@gmail.com', '0.00', '82539cf14d7cf4eae417b4aafc361900ef2e7c80', NULL, NULL, NULL, 1572689131, NULL, 0, 0, 'bnupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 15:35:31', NULL, NULL, 1),
(26, '::1', 'trupti57603@gmail.com', 'tp120125', '$2y$08$KKIdheRcn70Iuu.ssyWeuOx3nMoeRX2H2.l641rQNJ74XG9Fk6E9W', NULL, 'trupti57603@gmail.com', '0.00', 'f1d94bb384651549ff81a17f6b677e30e8a03afc', NULL, NULL, NULL, 1572689302, NULL, 0, 0, 'bnupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 15:38:22', NULL, NULL, 1),
(27, '::1', 'trupti59603@gmail.com', 'tp120126', '$2y$08$c6kNRkQDzUa0OS3YqyaAaessEKkY4qrhaPDGRcvUAfFX3ZtHWX0pe', NULL, 'trupti59603@gmail.com', '0.00', '133d4df17970c1c56f63f3da119feb733f433c27', NULL, NULL, NULL, 1572689519, NULL, 0, 0, 'bnupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 15:41:59', NULL, NULL, 1),
(28, '::1', 'trupti50603@gmail.com', 'tp120127', '$2y$08$Usn5.Kr96OMhLMvc5.P8KeJhG.3rnoH8cWF1CCV1IpLHpbSyHy15q', NULL, 'trupti50603@gmail.com', '0.00', 'ea1eefc46a11e2074c362b8c9f292d5d1ebb0916', NULL, NULL, NULL, 1572689699, NULL, 0, 0, 'bnupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 15:44:59', NULL, NULL, 1),
(29, '::1', 'trupti123@gmail.com', 'tp120128', '$2y$08$YBw4c2yM13bJgzHzh32UXuXAw2Xnkur1Mw0YvGddstPIVuWq3c4iS', NULL, 'trupti123@gmail.com', '0.00', '3b928c9334c5963f91bfe56770352096afaf07f6', NULL, NULL, NULL, 1572689760, NULL, 0, 0, 'bnupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 15:46:00', NULL, NULL, 1),
(30, '::1', 'trupti1111@gmail.com', 'tp120129', '$2y$08$hyJs6aKJ.UvdsBjFVGXjXO4ID9.f4KscqSk5/AeSdc.1kqcI9CNiC', NULL, 'trupti1111@gmail.com', '0.00', '5954bd5f2d012f3fbe441e63fb7d8bce4dc979d8', NULL, NULL, NULL, 1572691790, NULL, 0, 0, 'bnupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 16:19:50', NULL, NULL, 1),
(31, '::1', 'trupti111lk1@gmail.com', 'tp120131', '$2y$08$7zmBDVilajR27KIr.cF.YeYkMRYKkjEspPEwScva9w3R4Nb21/ZMm', NULL, 'trupti111lk1@gmail.com', '0.00', '306daa01bd1fffcf097d175ae189cd20407200ad', NULL, NULL, NULL, 1572694088, NULL, 0, 0, 'bnupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 16:58:08', NULL, NULL, 1),
(32, '::1', 'truptimk111lk1@gmail.com', 'tp120132', '$2y$08$Zsmi5mzFRo7cV6IaNKQZx.uPN/sR7w1gcLsmS5/QvHy4ZdaSqXtWy', NULL, 'truptimk111lk1@gmail.com', '0.00', '4bd24310a4b799207c7bfbc4f6e0f07b0cf7c23f', NULL, NULL, NULL, 1572694106, NULL, 0, 0, 'bnupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 16:58:26', NULL, NULL, 1),
(33, '::1', 'truptimdfgdfgdfddfg@gmail.com', 'tp120133', '$2y$08$dgL9m3zvxgNyEY3pf5e5eugXDSj5ftX0wJWbXPIYZT12luNLLPLfi', NULL, 'truptimdfgdfgdfddfg@gmail.com', '0.00', '68e9d85f966dc5a64891e5d9880fda03348a41d7', NULL, NULL, NULL, 1572694288, NULL, 0, 0, 'bnupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 17:01:28', NULL, NULL, 1),
(34, '::1', 'truptimdsfgdfgdfddfg@gmail.com', 'tp120134', '$2y$08$3FMfaJdOjdiQo8GpRrBTI.0AeFN2T0kthpdwPtG.FrDPzCNZVL0eq', NULL, 'truptimdsfgdfgdfddfg@gmail.com', '0.00', 'a136837a7dda589ec9b5610ca375d601c40c53aa', NULL, NULL, NULL, 1572697001, NULL, 0, 0, 'bnupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 17:46:41', NULL, NULL, 1),
(35, '::1', 'truptimdsfgdfgd1fddfg@gmail.com', 'tp120135', '$2y$08$ZnR.pk3kMZ.OAb4oE4rrMuA6caj0KzmTPkFr.KmAKjD2dWzqT58IS', NULL, 'truptimdsfgdfgd1fddfg@gmail.com', '0.00', '112c9b7b174bbe46c373f5ca86c598f43c44c811', NULL, NULL, NULL, 1572705655, NULL, 0, 0, 'bnupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 20:10:55', NULL, NULL, 1),
(36, '::1', 'truptimp@gmail.com', 'tp120136', '$2y$08$AOtXgh9LlXq4IQgaPKat.eGFO1ApvPevzuyPWdXdzsYDRnT11UsFO', NULL, 'truptimp@gmail.com', '0.00', 'ba6ff74712e776c7dd0ecdf102bcf8e7690706f8', NULL, NULL, NULL, 1572705737, NULL, 0, 0, 'bnupti', NULL, NULL, NULL, NULL, NULL, '2019-11-02 20:12:17', NULL, NULL, 1),
(37, '::1', 'bodaashok57125@gmail.com', 'tp120137', '$2y$08$lzy1a11y7BKY10RgEeZDwOLbgrlxv.m5xBiPWhiLX5FU52YXn/Tja', NULL, 'bodaashok57125@gmail.com', '0.00', '5c3fc767d1599f7e650a996d8beeb5be9163cdcb', NULL, NULL, NULL, 1572856968, NULL, 0, 0, 'boda ashok', NULL, NULL, NULL, NULL, NULL, '2019-11-04 14:12:48', NULL, NULL, 1),
(38, '::1', 'truptikmp@gmail.com', 'tp120139', '$2y$08$3SgN0HPqeBtGxs1jaIhDsur7RuW1V4js1DtIIHFDEWWjDbsuY22Ze', NULL, 'truptikmp@gmail.com', '0.00', '320c5dff202658b7da9ee5f680546b6e3906edec', NULL, NULL, NULL, 1573123951, NULL, 0, 0, 'bnupti', NULL, NULL, NULL, NULL, NULL, '2019-11-07 16:22:31', NULL, NULL, 1),
(39, '::1', 'shivajibanothu1@gmail.com', 'NCE0104', '$2y$08$h8eX3oicIpcIAz4cpzNTZeF70swZRJEJ7BNzg3kloBqllL1YDCKZ.', NULL, 'shivajibanothu1@gmail.com', '0.00', '11f2420944709a47b81e1de0ddcc62e211893f8b', NULL, NULL, NULL, 1575454309, NULL, 0, 0, 'shivaji banothu', NULL, NULL, NULL, NULL, NULL, '2019-12-04 15:41:49', NULL, NULL, 1),
(40, '::1', 'asnmdf@gmail.com', 'NCE0106', '$2y$08$KWJBz5x/59F5MDcpbGLfZ.ga7nciSQjU0etsM.NVdr48wDlzCYwE.', NULL, 'asnmdf@gmail.com', '0.00', '168795b5792653dc3ac4547434219092762397d1', NULL, NULL, NULL, 1575454360, NULL, 0, 0, 'terstp', NULL, NULL, NULL, NULL, NULL, '2019-12-04 15:42:40', NULL, NULL, 1),
(41, '::1', 'asmdf@gmail.com', 'NCE0108', '$2y$08$Nn5ZMc6HrKQ/9CejJ/YSue0vkfSPTeB0aABxDhgRJeR32Fg2ZGeUq', NULL, 'asmdf@gmail.com', '0.00', 'f4798a3f918fa128e4367f39674c89ef8504cb0c', NULL, NULL, NULL, 1575454377, NULL, 0, 0, 'terstp', NULL, NULL, NULL, NULL, NULL, '2019-12-04 15:42:57', NULL, NULL, 1),
(42, '::1', 'naveenkallepalli37@gmail.com', 'NCE0110', '$2y$08$.sx85FoFoEKhjCBxAN/7zePJGJXGlfAguScPwTL6gKnpjB0C4AZ9q', NULL, 'naveenkallepalli37@gmail.com', '0.00', '0d052182684259f7a22097f9b7f2f31607f40cb7', NULL, NULL, NULL, 1575454450, NULL, 0, 0, 'Kallepelli Naveen', NULL, NULL, NULL, NULL, NULL, '2019-12-04 15:44:10', NULL, NULL, 1),
(62, '::1', 'mail2swapnikareddy@gmail.com', 'NCE0137', '$2y$08$C7GGUvpLK5D2tj0JzJ5jHeWm156tas2942JMOGj5/iWcTNOJleTji', NULL, 'mail2swapnikareddy@gmail.com', '0.00', 'c21fcef8baaa07831b00f9405b58340d9be936f9', NULL, NULL, NULL, 1575463858, NULL, 0, 0, 'Swapnika reddy', NULL, NULL, NULL, NULL, NULL, '2019-12-04 18:20:58', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_address`
--

CREATE TABLE `users_address` (
  `id` bigint(50) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` longtext NOT NULL,
  `created_user_id` bigint(20) DEFAULT NULL,
  `updated_user_id` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0=deleted,1=Active,2=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(5, 1, 1),
(6, 1, 2),
(7, 1, 3),
(14, 1, 5),
(21, 4, 2),
(22, 7, 2),
(23, 8, 2),
(24, 33, 14),
(25, 34, 14),
(26, 35, 14),
(27, 36, 14),
(28, 37, 14),
(29, 38, 14),
(30, 39, 2),
(31, 40, 2),
(32, 41, 2),
(33, 42, 2),
(53, 62, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users_permissions`
--

CREATE TABLE `users_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `perm_id` int(11) NOT NULL,
  `value` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 = Deny, 1 = Allow',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_permissions`
--

INSERT INTO `users_permissions` (`id`, `user_id`, `perm_id`, `value`, `created_at`, `updated_at`) VALUES
(5, 1, 1, 1, 1568900539, 1568900539),
(6, 1, 2, 1, 1568900539, 1568900539);

-- --------------------------------------------------------

--
-- Table structure for table `user_services`
--

CREATE TABLE `user_services` (
  `id` bigint(20) NOT NULL,
  `vendor_id` bigint(20) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `created_user_id` bigint(20) DEFAULT NULL,
  `updated_user_id` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_services`
--

INSERT INTO `user_services` (`id`, `vendor_id`, `name`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`, `deleted_at`, `status`) VALUES
(1, 0, 'dsgdfg', 1, 1, '2019-12-10 14:38:12', '2019-12-10 14:42:12', '2019-12-10 14:42:23', 1),
(2, NULL, 'dfgfghjfgh', 1, 1, '2019-12-10 14:42:28', '2019-12-10 14:43:44', '2019-12-10 14:43:48', 1),
(3, 0, 'test', 1, 1, '2019-12-10 14:43:38', '2019-12-12 14:23:08', '0000-00-00 00:00:00', 1),
(4, 0, 'ghv hhhhhhhhhhhh', 1, NULL, '2019-12-10 14:49:41', NULL, '0000-00-00 00:00:00', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups_permissions`
--
ALTER TABLE `groups_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roleID_2` (`group_id`,`perm_id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permKey` (`perm_key`);

--
-- Indexes for table `riders`
--
ALTER TABLE `riders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_address`
--
ALTER TABLE `users_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `users_permissions`
--
ALTER TABLE `users_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userID` (`user_id`,`perm_id`);

--
-- Indexes for table `user_services`
--
ALTER TABLE `user_services`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `groups_permissions`
--
ALTER TABLE `groups_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=278;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `riders`
--
ALTER TABLE `riders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `users_address`
--
ALTER TABLE `users_address`
  MODIFY `id` bigint(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `users_permissions`
--
ALTER TABLE `users_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user_services`
--
ALTER TABLE `user_services`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
